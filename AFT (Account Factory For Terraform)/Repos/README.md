Repos (Modified) In the Folder and the URL of the base Repos 

AFT Setup Repo -

aws-control-tower-aft (https://github.com/hashicorp/learn-terraform-aws-control-tower-aft.git)

Account Creation and Customization Repos-

aft-global-customizations (https://github.com/hashicorp/learn-terraform-aft-global-customizations)

aft-account-request (https://github.com/hashicorp/learn-terraform-aft-account-request)

aft-account-provisioning-customizations (https://github.com/hashicorp/learn-terraform-aft-account-provisioning-customizations)

aft-account-customizations (https://github.com/hashicorp/learn-terraform-aft-account-customizations)