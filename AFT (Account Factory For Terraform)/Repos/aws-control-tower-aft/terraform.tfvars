# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

ct_management_account_id    = "560979642322"
log_archive_account_id      = "124234103830"
audit_account_id            = "270171728246"
aft_management_account_id   = "980112349415"
ct_home_region              = "eu-west-1"
tf_backend_secondary_region = "us-east-2"
