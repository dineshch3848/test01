module "iam" {
  source = "./modules/iam-baseline"
  minimum_password_length = var.minimum_password_length
  support_iam_role_name = var.support_iam_role_name
  support_iam_role_policy_name = var.support_iam_role_policy_name
  max_password_age = var.max_password_age
  password_reuse_prevention = var.password_reuse_prevention
  require_lowercase_characters = var.require_lowercase_characters
  require_numbers = var.require_numbers
  require_uppercase_characters = var.require_uppercase_characters
  require_symbols = var.require_symbols
  hard_expiry = var.hard_expiry
  allow_users_to_change_password = var.allow_users_to_change_password
  create_password_policy = var.create_password_policy
  create_support_role = var.create_support_role
  tags = var.tags
  
}