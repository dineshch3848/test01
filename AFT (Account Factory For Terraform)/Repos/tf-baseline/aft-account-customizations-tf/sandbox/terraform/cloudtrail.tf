module "cloudtrail" {
  source = "./modules/cloudtrail-baseline"
  cloudtrail_enabled = var.cloudtrail_enabled
  cloudtrail_name = var.cloudtrail_name
  cloudtrail_sns_topic_enabled = var.cloudtrail_sns_topic_enabled
  cloudtrail_sns_topic_name = var.cloudtrail_sns_topic_name
  cloudwatch_logs_enabled = var.cloudwatch_logs_enabled
  cloudwatch_logs_group_name = var.cloudwatch_logs_group_name
  cloudwatch_logs_retention_in_days = var.cloudwatch_logs_retention_in_days
  iam_role_name = var.iam_role_name
  iam_role_policy_name = var.iam_role_policy_name
  key_deletion_window_in_days = var.key_deletion_window_in_days
  CloudTrailBucketName = var.CloudTrailBucketName
  is_organization_trail = var.is_organization_trail
  s3_object_level_logging_buckets = var.s3_object_level_logging_buckets
  tags = var.tags
  
}