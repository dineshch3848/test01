module "analyzer" {
  source = "./modules/analyzer-baseline"
  analyzer_enabled = var.analyzer_enabled
  analyzer_name = var.analyzer_name
  is_organization = var.is_organization
  tags = var.tags  
}