variable "public_access_enable" {
    description = "It is required for restricting the public access of the s3 bucket objects"
    type = bool
    default = true  
}

variable "block_public_acls" {
    description = "It is required for restricting the public access of the s3 bucket objects by blocking public acls"
    type = bool
    default = true  
}

variable "block_public_policy" {
    description = "It is required for restricting the public access of the s3 bucket objects by blocking public policy of the s3 service"
    type = bool
    default = true  
}

variable "ignore_public_acls" {
    description = "It is required for restricting the public access of the s3 bucket objects by blocking public policy of the s3 service"
    type = bool
    default = true  
}

variable "restrict_public_buckets" {
    description = "It is required for restricting the public access of the s3 bucket objects by blocking public buckets of the s3 buckets"
    type = bool
    default = true     
  
}