output "s3_account_public_access_block_id" {
  value = aws_s3_account_public_access_block.public_block[*].id
}