cloudtrail_enabled = true
cloudtrail_name = "cloudtrail_multi_region"
cloudtrail_sns_topic_enabled = true
cloudtrail_sns_topic_name = "cloudtrail-multi-region-sns-topic"
cloudwatch_logs_enabled = true
cloudwatch_logs_group_name = "cloudtrail-multi-region"
cloudwatch_logs_retention_in_days = 365
iam_role_name = "CloudTrail-CloudWatch-Delivery-Role"
iam_role_policy_name = "CloudTrail-CloudWatch-Delivery-Policy"
key_deletion_window_in_days = 10
s3_bucket_name = "cloudtrail-s3-buck"
is_organization_trail = false
s3_object_level_logging_buckets = ["arn:aws:s3:::"]
tags = {
  Terraform = true
}