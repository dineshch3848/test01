output "cloudtrail" {
  description = "The trail for recording events in all regions."
  value       = var.cloudtrail_enabled? aws_cloudtrail.global[*] : null
}

output "cloudtrail_sns_topic" {
  description = "The sns topic linked to the cloudtrail."
  value       = var.cloudtrail_sns_topic_enabled && var.cloudtrail_enabled ? aws_sns_topic.cloudtrail-sns-topic[*] : null
}

output "kms_key" {
  description = "The  KMS key used for encrypting CloudTrail events."
  value       = var.cloudtrail_enabled ? aws_kms_key.cloudtrail[*] : null
}

output "log_delivery_iam_role" {
  description = "The IAM role used for delivering CloudTrail events to CloudWatch Logs."
  value       = var.cloudwatch_logs_enabled && var.cloudtrail_enabled ? aws_iam_role.cloudwatch_delivery[*] : null
}

output "log_group" {
  description = "The CloudWatch Logs log group which stores CloudTrail events."
  value       = var.cloudwatch_logs_enabled && var.cloudtrail_enabled ? aws_cloudwatch_log_group.cloudtrail_events[*].name : null
}
