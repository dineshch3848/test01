output "analyzer_id" {
  value = aws_accessanalyzer_analyzer.default[*].id
}

output "analyzer_arn" {
  value = aws_accessanalyzer_analyzer.default[*].arn
}