minimum_password_length = 14
support_iam_role_name = "IAM-Support"
support_iam_role_policy_name = "IAM-Support-Role"
max_password_age = 90
password_reuse_prevention = 24
require_lowercase_characters = true
require_numbers = true
require_uppercase_characters = true
require_symbols = true
hard_expiry = true
allow_users_to_change_password = true
create_password_policy = true
create_support_role = true
tags = {
  Terraform = true
}


