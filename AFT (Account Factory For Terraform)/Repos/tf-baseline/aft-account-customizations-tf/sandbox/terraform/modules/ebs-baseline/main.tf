# --------------------------------------------------------------------------------------------------
# Enable Default EBS Encryption
# --------------------------------------------------------------------------------------------------
resource "aws_ebs_encryption_by_default" "ebs_encrypt" {
  count = var.ebs_encrypt_enabled ? 1 : 0
  enabled = true
}
