#===================================================================================
#Analyzer-Baselines Variables
#===================================================================================
variable "analyzer_enabled" {
  description = "The boolean flag whether this module is enabled or not. No resources are created when set to false."
  type = bool
  default     = true
}

variable "analyzer_name" {
  description = "The name for the IAM Access Analyzer resource to be created."
  type = string
  default     = "default-analyer"
}


variable "is_organization" {
  description = "The boolean flag whether this module is configured for the organization master account or the individual account."
  default     = false
}

#===================================================================================
#Cloudtrail-Baselines Variables
#===================================================================================
variable "cloudtrail_enabled" {
  description = "The boolean flag whether this module is enabled or not. No resources are created when set to false."
  type = bool
  default     = true
}

variable "cloudtrail_name" {
  description = "The name of the trail."
  default     = "cloudtrail-multi-region"
  type = string
}

variable "cloudtrail_sns_topic_enabled" {
  description = "Specifies whether the trail is delivered to a SNS topic."
  type = bool
  default     = true
}

variable "cloudtrail_sns_topic_name" {
  description = "The SNS topic linked to the CloudTrail"
  type = string
  default     = "cloudtrail-multi-region-sns-topic"
}

variable "cloudwatch_logs_enabled" {
  description = "Specifies whether the trail is delivered to CloudWatch Logs."
  type = bool
  default     = true
}

variable "cloudwatch_logs_group_name" {
  description = "The name of CloudWatch Logs group to which CloudTrail events are delivered."
  type = string
  default     = "cloudtrail-multi-region"
}

variable "cloudwatch_logs_retention_in_days" {
  description = "Number of days to retain logs for. CIS recommends 365 days.  Possible values are: 0, 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653. Set to 0 to keep logs indefinitely."
  default     = 365
  type = number
}

variable "iam_role_name" {
  description = "The name of the IAM Role to be used by CloudTrail to delivery logs to CloudWatch Logs group."
  default     = "CloudTrail-CloudWatch-Delivery-Role"
  type = string
}

variable "iam_role_policy_name" {
  description = "The name of the IAM Role Policy to be used by CloudTrail to delivery logs to CloudWatch Logs group."
  default     = "CloudTrail-CloudWatch-Delivery-Policy"
  type = string
}

variable "key_deletion_window_in_days" {
  description = "Duration in days after which the key is deleted after destruction of the resource, must be between 7 and 30 days. Defaults to 30 days."
  type = number
  default     = 10

}

variable "CloudTrailBucketName" {
  description = "s3 bucket for cloudtrail"
  type = string
  default = "cloudtrail-bucket-eu-april07"
}

variable "is_organization_trail" {
  description = "Specifies whether the trail is an AWS Organizations trail. Organization trails log events for the master account and all member accounts. Can only be created in the organization master account."
  type = bool
  default     = false
}

variable "s3_object_level_logging_buckets" {
  description = "The list of S3 bucket ARNs on which to enable object-level logging."
  default     = ["arn:aws:s3:::"] # All S3 buckets
  type = list(string)
}

#===================================================================================
#EBS-Baselines Variables
#===================================================================================

variable "ebs_encrypt_enabled" {
  description = "The boolean flag whether this module is enabled or not. No resources are created when set to false."
  default     = true
  type = bool
}

#===================================================================================
#IAM-Baselines Variables
#===================================================================================

variable "support_iam_role_name" {
  description = "The name of the the support role."
  default     = "IAM-Support"
  type = string
}

variable "support_iam_role_policy_name" {
  description = "The name of the support role policy."
  default     = "IAM-Support-Role"
  type = string
}
variable "max_password_age" {
  description = "The number of days that an user password is valid."
  default     = 90
  type = string
}

variable "minimum_password_length" {
  description = "Minimum length to require for user passwords."
  default     = 14
  type = number
}

variable "password_reuse_prevention" {
  description = "The number of previous passwords that users are prevented from reusing."
  default     = 24
  type = number
}

variable "require_lowercase_characters" {
  description = "Whether to require lowercase characters for user passwords."
  default     = true
  type = bool
}

variable "require_numbers" {
  description = "Whether to require numbers for user passwords."
  default     = true
  type = bool
}

variable "require_uppercase_characters" {
  description = "Whether to require uppercase characters for user passwords."
  default     = true
}

variable "require_symbols" {
  description = "Whether to require symbols for user passwords."
  default     = true
}

variable "allow_users_to_change_password" {
  description = "Whether to allow users to change their own password."
  default     = true
}

variable "create_password_policy" {
  type        = bool
  description = "Define if the password policy should be created."
  default     = true
}

variable "create_support_role" {
  type        = bool
  description = "Define if the support role should be created."
  default     = true
}

variable "hard_expiry"{
  type        = bool
  description = "Define if the hard expiry of the password is enabled."
  default     = true

}

#===================================================================================
#S3-Baselines Variables
#===================================================================================

variable "public_access_enable" {
    description = "It is required for restricting the public access of the s3 bucket objects"
    type = bool
    default = true  
}

variable "block_public_acls" {
    description = "It is required for restricting the public access of the s3 bucket objects by blocking public acls"
    type = bool
    default = true  
}

variable "block_public_policy" {
    description = "It is required for restricting the public access of the s3 bucket objects by blocking public policy of the s3 service"
    type = bool
    default = true  
}

variable "ignore_public_acls" {
    description = "It is required for restricting the public access of the s3 bucket objects by blocking public policy of the s3 service"
    type = bool
    default = true  
}

variable "restrict_public_buckets" {
    description = "It is required for restricting the public access of the s3 bucket objects by blocking public buckets of the s3 buckets"
    type = bool
    default = true     
  
}

#===================================================================================
#Default Variables
#===================================================================================
variable "tags" {
  description = "Specifies object tags key and value. This applies to all resources created by this module."
  default = {
    "Terraform" = true
  }
}
