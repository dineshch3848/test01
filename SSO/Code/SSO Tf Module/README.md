<!-- BEGIN_TF_DOCS -->
SSO Module

This module handles the creation of SSO of an AWS Account.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >=4.0.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_account-assignments"></a> [account-assignments](#module\_account-assignments) | ./modules/account-assignments | n/a |
| <a name="module_identitystore"></a> [identitystore](#module\_identitystore) | ./modules/identitystore | n/a |
| <a name="module_permission-sets"></a> [permission-sets](#module\_permission-sets) | ./modules/permission-sets | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_accountassignments"></a> [accountassignments](#input\_accountassignments) | accountassignments = [{<br>    permission\_set\_arn : "(Required, Forces new resource) The Amazon Resource Name (ARN) of the Permission Set that the admin wants to grant the principal access to"<br>    attribute\_value : "(Required) An identifier for an object in SSO, such as a user name or group name"<br>    principal\_type : "(Required, Forces new resource) The entity type for which the assignment will be created. Valid values: USER, GROUP."<br>    target\_id : "(Required, Forces new resource) An AWS account identifier, typically a 10-12 digit string"<br>  }] | <pre>list(object({<br>    permission_set_arn = string<br>    attribute_value    = string<br>    principal_type     = string<br>    target_id          = string<br>  }))</pre> | n/a | yes |
| <a name="input_groups"></a> [groups](#input\_groups) | groups = [{<br>    display\_name : "(Optional) A string containing the name of the group"<br>    description : "(Optional) A string containing the description of the group"<br>  }] | <pre>list(object({<br>    display_name = optional(string)<br>    description  = optional(string)<br>  }))</pre> | n/a | yes |
| <a name="input_permissionsets"></a> [permissionsets](#input\_permissionsets) | permissionsets = [{<br>    name : "(Required, Forces new resource) The name of the Permission Set"<br>    description : "(Optional) The description of the Permission Set"<br>    relay\_state : "(Optional) The relay state URL used to redirect users within the application during the federation authentication process"<br>    session\_duration : "(Optional) The length of time that the application user sessions are valid in the ISO-8601 standard"<br>    setpolicy : "(Optional) Whether to set inline set policy for the permission set"<br>    policy\_path : "(Optional) Path where the policy is located"<br>    attachmanagedpolicy : "(Optional) Whether to attach a managed policy"<br>    policy\_arn : "(Optional) ARN of the policy to attach"<br>    attachcustmanagedpolicy : "(Optional) Whether to attach a customer managed policy"<br>    policy\_name : "(Optional) Policy name exactly as it appears in the IAM console"<br>    policy\_path : "(Optional) Path of the customer managed policy"<br>  }] | <pre>list(object({<br>    name                    = string<br>    description             = optional(string)<br>    relay_state             = optional(string)<br>    session_duration        = optional(string)<br>    setpolicy               = optional(bool)<br>    policy_path             = optional(string)<br>    attachmanagedpolicy     = optional(bool)<br>    policy_arn              = optional(string)<br>    attachcustmanagedpolicy = optional(bool)<br>    policy_name             = optional(string)<br>    policy_path             = optional(string)<br>  }))</pre> | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | Region where to deploy SSO | `string` | n/a | yes |
| <a name="input_users"></a> [users](#input\_users) | users = [{<br>    display\_name : "(Required) The name that is typically displayed when the user is referenced"<br>    user\_name : "(Required, Forces new resource) A unique string used to identify the user"<br>    given\_name : "(Required) The given name of the user"<br>    family\_name : "(Required) The family name of the user"<br>    email : "(Optional) Details about the user's email"<br>  }] | <pre>list(object({<br>    display_name = string<br>    user_name    = string<br>    given_name   = string<br>    family_name  = string<br>    email        = optional(string)<br>  }))</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_assignments"></a> [assignments](#output\_assignments) | The identifier of the Account Assignment |
| <a name="output_group_id"></a> [group\_id](#output\_group\_id) | The identifier of the newly created group in the identity store |
| <a name="output_id"></a> [id](#output\_id) | The Amazon Resource Names (ARNs) of the Permission Set and SSO Instance, separated by a comma |
| <a name="output_user_id"></a> [user\_id](#output\_user\_id) | The identifier for this user in the identity store |
<!-- END_TF_DOCS -->