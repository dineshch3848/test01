output "id" {
  description = "The Amazon Resource Names (ARNs) of the Permission Set and SSO Instance, separated by a comma"
  value       = module.permission-sets.id[*]
}