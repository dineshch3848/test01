/*
  * SSO Module
  *
  * This module handles the creation of SSO of an AWS Account.
  * 
*/

module "permission-sets" {
  source         = "../../modules/permission-sets"
  permissionsets = var.permissionsets
  region         = var.region
}