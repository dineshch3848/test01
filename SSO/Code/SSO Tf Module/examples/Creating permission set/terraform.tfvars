permissionsets = [
  {
    name        = "PermissionSet-S3"
    policy_path = "/../../policy/policy.json"
    setpolicy   = true
  },
  {
    name                = "AmazonEC2FullAccess"
    attachmanagedpolicy = true
    policy_arn          = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
  }
]

region = "eu-west-1"