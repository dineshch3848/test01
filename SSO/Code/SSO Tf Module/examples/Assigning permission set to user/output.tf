output "assignments" {
  description = "The identifier of the Account Assignment"
  value       = module.account-assignments.assignments[*]
}
