/*
  * SSO Module
  *
  * This module handles the creation of SSO of an AWS Account.
  * 
*/
/*
  * SSO Module
  *
  * This module handles the creation of SSO of an AWS Account.
  * 
*/

module "account-assignments" {
  source             = "../../modules/account-assignments"
  accountassignments = var.accountassignments
  region             = var.region
}