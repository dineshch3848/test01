users = [ ]

groups = [
  {
    display_name = "DevOps_Admins"
  },
  {
    display_name = "Admin"
    description  = "Full access for all services"
  }
]

region = "eu-west-1"