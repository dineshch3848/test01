output "group_id" {
  description = "The identifier of the newly created group in the identity store"
  value       = module.identitystore.group_id[*]
}
