/*
  * SSO Module
  *
  * This module handles the creation of SSO of an AWS Account.
  * 
*/

module "identitystore" {
  source = "../../modules/identitystore"
  users  = var.users
  groups = var.groups
  region = var.region
}