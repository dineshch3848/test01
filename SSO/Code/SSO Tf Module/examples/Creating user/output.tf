output "user_id" {
  description = "The identifier for this user in the identity store"
  value       = module.identitystore.user_id[*]
}