users = [
  {
    display_name = "Harshit"
    email        = "hvinze123@gmail.com"
    family_name  = "Vinze"
    given_name   = "Harshit"
    user_name    = "Harshit"
  },
  {
    display_name = "Siddharth"
    family_name  = "Mishra"
    given_name   = "Siddharth"
    user_name    = "Siddharth"
  }
]

groups = [ ]

region = "eu-west-1"