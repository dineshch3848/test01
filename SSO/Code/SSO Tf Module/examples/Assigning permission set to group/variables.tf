variable "accountassignments" {
  type = list(object({
    permission_set_arn = string
    attribute_value    = string
    principal_type     = string
    target_id          = string
  }))
  description = <<EOT
  accountassignments = [{
    permission_set_arn : "(Required, Forces new resource) The Amazon Resource Name (ARN) of the Permission Set that the admin wants to grant the principal access to"
    attribute_value : "(Required) An identifier for an object in SSO, such as a user name or group name"
    principal_type : "(Required, Forces new resource) The entity type for which the assignment will be created. Valid values: USER, GROUP."
    target_id : "(Required, Forces new resource) An AWS account identifier, typically a 10-12 digit string"
  }]
  EOT
}

variable "region" {
  description = "Region where to deploy SSO"
  type        = string
}
