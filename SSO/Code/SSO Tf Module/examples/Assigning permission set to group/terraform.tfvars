accountassignments = [
  {
    permission_set_arn = "arn:aws:sso:::permissionSet/ssoins-68049349d5305121/ps-7a3de3cd6bc2b0bf"
    attribute_value    = "LandingZone"
    principal_type     = "GROUP"
    target_id          = "560979642322"
  }
]

region = "eu-west-1"
