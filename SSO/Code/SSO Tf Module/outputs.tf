output "user_id" {
  description = "The identifier for this user in the identity store"
  value       = module.identitystore.user_id[*]
}

output "group_id" {
  description = "The identifier of the newly created group in the identity store"
  value       = module.identitystore.group_id[*]
}

output "assignments" {
  description = "The identifier of the Account Assignment"
  value       = module.account-assignments.assignments[*]
}

output "id" {
  description = "The Amazon Resource Names (ARNs) of the Permission Set and SSO Instance, separated by a comma"
  value       = module.permission-sets.id[*]
}