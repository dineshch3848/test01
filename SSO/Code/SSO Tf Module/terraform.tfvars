users = [
  {
    display_name = "Harshit"
    email        = "hvinze123@gmail.com"
    family_name  = "Vinze"
    given_name   = "Harshit"
    user_name    = "Harshit"
  },
  {
    display_name = "Siddharth"
    family_name  = "Mishra"
    given_name   = "Siddharth"
    user_name    = "Siddharth"
  },
  {
    display_name = "Test_User"
    family_name  = "User"
    given_name   = "Test"
    user_name    = "Test_User"
  }
]

groups = [
  {
    display_name = "DevOps_Admins"
  },
  {
    display_name = "Admin"
    description  = "Full access for all services"
  }
]

accountassignments = [
  {
    permission_set_arn = "arn:aws:sso:::permissionSet/ssoins-68049349d5305121/ps-7a3de3cd6bc2b0bf"
    attribute_value    = "harshit.vinze@yash.com"
    principal_type     = "USER"
    target_id          = "560979642322"
  },
  {
    permission_set_arn = "arn:aws:sso:::permissionSet/ssoins-68049349d5305121/ps-ff44354439a5941c"
    attribute_value    = "aarav.bairagi@yash.com"
    principal_type     = "USER"
    target_id          = "560979642322"
  },
  {
    permission_set_arn = "arn:aws:sso:::permissionSet/ssoins-68049349d5305121/ps-ed4ae9415f1fc262"
    attribute_value    = "biswajit.mishra@yash.com"
    principal_type     = "USER"
    target_id          = "560979642322"
  },
  {
    permission_set_arn = "arn:aws:sso:::permissionSet/ssoins-68049349d5305121/ps-7a3de3cd6bc2b0bf"
    attribute_value    = "LandingZone"
    principal_type     = "GROUP"
    target_id          = "560979642322"
  }
]

permissionsets = [
  {
    name        = "PermissionSet-S3"
    policy_path = "./policy/policy.json"
    setpolicy   = true
  },
  {
    name        = "PermissionSet-ec2startstop"
    policy_path = "./policy/ec2startstopPolicy.json"
    setpolicy   = true
  },
  {
    name        = "PermissionSet-securityadmin"
    policy_path = "./policy/SecurityAdminPolicy.json"
    setpolicy   = true
  },
  {
    name                = "AmazonEC2FullAccess"
    attachmanagedpolicy = true
    policy_arn          = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
  }
]

region = "eu-west-1"
