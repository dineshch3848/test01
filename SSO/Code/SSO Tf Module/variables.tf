variable "users" {
  type = list(object({
    display_name = string
    user_name    = string
    given_name   = string
    family_name  = string
    email        = optional(string)
  }))
  description = <<EOT
  users = [{
    display_name : "(Required) The name that is typically displayed when the user is referenced"
    user_name : "(Required, Forces new resource) A unique string used to identify the user"
    given_name : "(Required) The given name of the user"
    family_name : "(Required) The family name of the user"
    email : "(Optional) Details about the user's email"
  }]
  EOT
}

variable "groups" {
  type = list(object({
    display_name = optional(string)
    description  = optional(string)
  }))
  description = <<EOT
  groups = [{
    display_name : "(Optional) A string containing the name of the group"
    description : "(Optional) A string containing the description of the group"
  }]
  EOT
}

variable "accountassignments" {
  type = list(object({
    permission_set_arn = string
    attribute_value    = string
    principal_type     = string
    target_id          = string
  }))
  description = <<EOT
  accountassignments = [{
    permission_set_arn : "(Required, Forces new resource) The Amazon Resource Name (ARN) of the Permission Set that the admin wants to grant the principal access to"
    attribute_value : "(Required) An identifier for an object in SSO, such as a user name or group name"
    principal_type : "(Required, Forces new resource) The entity type for which the assignment will be created. Valid values: USER, GROUP."
    target_id : "(Required, Forces new resource) An AWS account identifier, typically a 10-12 digit string"
  }]
  EOT
}

variable "permissionsets" {
  type = list(object({
    name                    = string
    description             = optional(string)
    relay_state             = optional(string)
    session_duration        = optional(string)
    setpolicy               = optional(bool)
    policy_path             = optional(string)
    attachmanagedpolicy     = optional(bool)
    policy_arn              = optional(string)
    attachcustmanagedpolicy = optional(bool)
    policy_name             = optional(string)
    policy_path             = optional(string)
  }))
  description = <<EOT
  permissionsets = [{
    name : "(Required, Forces new resource) The name of the Permission Set"
    description : "(Optional) The description of the Permission Set"
    relay_state : "(Optional) The relay state URL used to redirect users within the application during the federation authentication process"
    session_duration : "(Optional) The length of time that the application user sessions are valid in the ISO-8601 standard"
    setpolicy : "(Optional) Whether to set inline set policy for the permission set"
    policy_path : "(Optional) Path where the policy is located"
    attachmanagedpolicy : "(Optional) Whether to attach a managed policy"
    policy_arn : "(Optional) ARN of the policy to attach"
    attachcustmanagedpolicy : "(Optional) Whether to attach a customer managed policy"
    policy_name : "(Optional) Policy name exactly as it appears in the IAM console"
    policy_path : "(Optional) Path of the customer managed policy"
  }]
  EOT
}

variable "region" {
  description = "Region where to deploy SSO"
  type        = string
}
