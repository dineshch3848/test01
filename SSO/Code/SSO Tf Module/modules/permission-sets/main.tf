data "aws_ssoadmin_instances" "example" {}

resource "aws_ssoadmin_permission_set" "example" {
  for_each = { for key, value in var.permissionsets : key => value }

  name             = each.value.name
  description      = each.value.description
  instance_arn     = tolist(data.aws_ssoadmin_instances.example.arns)[0]
  relay_state      = each.value.relay_state
  session_duration = each.value.session_duration
}

resource "aws_ssoadmin_permission_set_inline_policy" "example" {
  for_each = { for key, value in var.permissionsets : key => value if value.setpolicy != null }

  inline_policy      = data.aws_iam_policy_document.example[each.key].json
  instance_arn       = tolist(data.aws_ssoadmin_instances.example.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.example[each.key].arn
}

data "aws_iam_policy_document" "example" {
  for_each = { for key, value in var.permissionsets : key => value if value.setpolicy != null }

  source_policy_documents = [file(each.value.policy_path)]
}

resource "aws_ssoadmin_managed_policy_attachment" "this" {
  for_each = { for key, value in var.permissionsets : key => value if value.attachmanagedpolicy != null }

  instance_arn       = tolist(data.aws_ssoadmin_instances.example.arns)[0]
  managed_policy_arn = each.value.policy_arn
  permission_set_arn = aws_ssoadmin_permission_set.example[each.key].arn
}

resource "aws_ssoadmin_customer_managed_policy_attachment" "this" {
  for_each = { for key, value in var.permissionsets : key => value if value.attachcustmanagedpolicy != null }

  instance_arn       = tolist(data.aws_ssoadmin_instances.example.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.example[each.key].arn

  customer_managed_policy_reference {
    name = each.value.policy_name
    path = each.value.policy_path
  }
}
