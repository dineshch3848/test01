variable "permissionsets" {
  type = list(object({
    name                    = string
    description             = optional(string)
    relay_state             = optional(string)
    session_duration        = optional(string)
    setpolicy               = optional(bool)
    policy_path             = optional(string)
    attachmanagedpolicy     = optional(bool)
    policy_arn              = optional(string)
    attachcustmanagedpolicy = optional(bool)
    policy_name             = optional(string)
    policy_path             = optional(string)
  }))
  description = <<EOT
  permissionsets = [{
    name : "(Required, Forces new resource) The name of the Permission Set"
    description : "(Optional) The description of the Permission Set"
    relay_state : "(Optional) The relay state URL used to redirect users within the application during the federation authentication process"
    session_duration : "(Optional) The length of time that the application user sessions are valid in the ISO-8601 standard"
    setpolicy : "(Optional) Whether to set inline set policy for the permission set"
    policy_path : "(Optional) Path where the policy is located"
    attachmanagedpolicy : "(Optional) Whether to attach a managed policy"
    policy_arn : "(Optional) ARN of the policy to attach"
    attachcustmanagedpolicy : "(Optional) Whether to attach a customer managed policy"
    policy_name : "(Optional) Policy name exactly as it appears in the IAM console"
    policy_path : "(Optional) Path of the customer managed policy"
  }]
  EOT
}

variable "region" {
  description = "Region where to deploy SSO"
  type        = string
}
