output "id" {
  description = "The Amazon Resource Names (ARNs) of the Permission Set and SSO Instance, separated by a comma"
  value = [for val in aws_ssoadmin_permission_set.example : val.id]
}