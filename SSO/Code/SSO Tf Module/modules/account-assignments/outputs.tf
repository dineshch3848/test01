output "assignments" {
  value = [for val in aws_ssoadmin_account_assignment.example : val.id]
  description = "The identifier of the Account Assignment"
}