data "aws_ssoadmin_instances" "example" {}

data "aws_identitystore_group" "this" {
  identity_store_id = tolist(data.aws_ssoadmin_instances.example.identity_store_ids)[0]
  for_each          = { for key, value in var.accountassignments : key => value if value.principal_type == "GROUP"}

  alternate_identifier {
    unique_attribute {
      attribute_path  = "DisplayName"
      attribute_value = each.value.attribute_value
    }
  }
}

data "aws_identitystore_user" "this" {
  identity_store_id = tolist(data.aws_ssoadmin_instances.example.identity_store_ids)[0]
  for_each          = { for key, value in var.accountassignments : key => value if value.principal_type != "GROUP"}

  alternate_identifier {
    unique_attribute {
      attribute_path  = "UserName"
      attribute_value = each.value.attribute_value
    }
  }
}

resource "aws_ssoadmin_account_assignment" "example" {
  instance_arn = tolist(data.aws_ssoadmin_instances.example.arns)[0]
  for_each     = { for key, value in var.accountassignments : key => value }

  permission_set_arn = each.value.permission_set_arn

  principal_id   = each.value.principal_type == "GROUP" ? data.aws_identitystore_group.this[each.key].id : data.aws_identitystore_user.this[each.key].id
  principal_type = each.value.principal_type

  target_id   = each.value.target_id
  target_type = "AWS_ACCOUNT"
}
