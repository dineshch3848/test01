variable "users" {
  type = list(object({
    display_name = string
    user_name    = string
    given_name   = string
    family_name  = string
    email        = optional(string)
  }))
  description = <<EOT
  users = [{
    display_name : "(Required) The name that is typically displayed when the user is referenced"
    user_name : "(Required, Forces new resource) A unique string used to identify the user"
    given_name : "(Required) The given name of the user"
    family_name : "(Required) The family name of the user"
    email : "(Optional) Details about the user's email"
  }]
  EOT
}

variable "groups" {
  type = list(object({
    display_name = optional(string)
    description  = optional(string)
  }))
  description = <<EOT
  groups = [{
    display_name : "(Optional) A string containing the name of the group"
    description : "(Optional) A string containing the description of the group"
  }]
  EOT
}

variable "region" {
  description = "Region where to deploy SSO"
  type        = string
}
