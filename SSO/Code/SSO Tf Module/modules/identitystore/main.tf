data "aws_ssoadmin_instances" "example" {}

resource "aws_identitystore_user" "example" {
  for_each = { for key, value in var.users : key => value }
  identity_store_id = tolist(data.aws_ssoadmin_instances.example.identity_store_ids)[0]

  display_name = each.value.display_name
  user_name    = each.value.user_name

  name {
    given_name  = each.value.given_name
    family_name = each.value.family_name
  }

  emails {
    value = each.value.email
  }
}

resource "aws_identitystore_group" "this" {
  for_each = { for key, value in var.groups : key => value }
  identity_store_id = tolist(data.aws_ssoadmin_instances.example.identity_store_ids)[0]

  display_name      = each.value.display_name
  description       = each.value.description
}
