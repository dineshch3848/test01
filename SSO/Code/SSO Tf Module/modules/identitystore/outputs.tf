output "user_id" {
  description = "The identifier for this user in the identity store"
  value = [for val in aws_identitystore_user.example : val.user_id]
}

output "group_id" {
  description = "The identifier of the newly created group in the identity store"
  value = [for val in aws_identitystore_group.this : val.group_id]
}
