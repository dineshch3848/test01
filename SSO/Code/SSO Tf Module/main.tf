/*
  * SSO Module
  *
  * This module handles the creation of SSO of an AWS Account.
  * 
*/

module "identitystore" {
  source = "./modules/identitystore"
  users  = var.users
  groups = var.groups
  region = var.region
}

module "account-assignments" {
  source             = "./modules/account-assignments"
  accountassignments = var.accountassignments
  region             = var.region
}

module "permission-sets" {
  source         = "./modules/permission-sets"
  permissionsets = var.permissionsets
  region         = var.region
}
