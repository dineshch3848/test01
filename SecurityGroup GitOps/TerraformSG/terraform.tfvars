security_groups = [
  {
    description = "Test SG"
    name_prefix = "test-sg"
    sg_egress_rules = [
      {
        from_port   = 0
        to_port     = 0
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
      }
    ]
    sg_ingress_rules = [
      {
        description = "http"
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        self        = false
      },
      {
        description = "ssh"
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }
    ]
    vpc_id = "vpc-0923b232212b0d9d0"
  },
  {
    description = "Test SG"
    name_prefix = "test2-sg"
    sg_ingress_rules = [
      {
        description = "https"
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        self        = false
      },
    ]
    sg_egress_rules = [
      {
        from_port   = 0
        to_port     = 0
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
      }
    ]
    vpc_id = "vpc-0923b232212b0d9d0"
  }
]

default_tags = {
  "Environment" = "Dev"
  "Owner"       = "harshit.vinze@yash.com"
}

region = "us-west-2"

role_arn = "arn:aws:iam::906890597400:role/assumed-role"
