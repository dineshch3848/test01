terraform {
  backend "s3" {
    bucket = "gitops-securitygroup-state"
    key    = "GitOpsSG/terraform.tfstate"
    region = "us-west-2"
  }
}
