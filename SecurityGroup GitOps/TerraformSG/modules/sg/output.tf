output "id" {
  description = "Security Groups Ids"
  value = [for val in aws_security_group.security_group : val.id]
} 