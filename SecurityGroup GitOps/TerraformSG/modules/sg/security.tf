resource "aws_security_group" "security_group" {
  for_each = { for key, val in var.security_groups : key => val }

  vpc_id      = lookup(each.value, "vpc_id", null)
  name        = lookup(each.value, "name_prefix", null)
  description = lookup(each.value, "description", null)
  tags = merge(
    var.default_tags,
    {
      Name = each.value.name_prefix
  })
  dynamic "ingress" {
    for_each = each.value.sg_ingress_rules != null ? each.value.sg_ingress_rules : []
    content {
      description      = lookup(ingress.value, "description", null)
      from_port        = lookup(ingress.value, "from_port", null)
      to_port          = lookup(ingress.value, "to_port", null)
      prefix_list_ids  = lookup(ingress.value, "prefix_list_ids", null)
      protocol         = ingress.value.protocol
      cidr_blocks      = lookup(ingress.value, "cidr_blocks", null)
      ipv6_cidr_blocks = lookup(ingress.value, "ipv6_cidr_blocks", null)
      security_groups  = lookup(ingress.value, "security_groups", null)
      self             = lookup(ingress.value, "self", null)
    }
  }

  dynamic "egress" {
    for_each = each.value.sg_egress_rules != null ? each.value.sg_egress_rules : []
    content {
      description      = lookup(egress.value, "description", null)
      from_port        = lookup(egress.value, "from_port", null)
      to_port          = lookup(egress.value, "to_port", null)
      prefix_list_ids  = lookup(egress.value, "prefix_list_ids", null)
      protocol         = egress.value.protocol
      cidr_blocks      = lookup(egress.value, "cidr_blocks", null)
      ipv6_cidr_blocks = lookup(egress.value, "ipv6_cidr_blocks", null)
      security_groups  = lookup(egress.value, "security_groups", null)
      self             = lookup(egress.value, "self", null)
    }
  }
}
