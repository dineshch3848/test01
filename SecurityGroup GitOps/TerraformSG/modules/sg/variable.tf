variable "security_groups" {
  description = "List of security group"
  type = list(object({
    vpc_id    = string
    name_prefix = string
    description = string
    sg_ingress_rules = optional(list(object({
      cidr_blocks      = optional(list(string)) # List of IPv4 CIDR Blocks
      ipv6_cidr_blocks = optional(list(string)) # List of IPv6 CIDR Blocks
      prefix_list_ids  = optional(list(string)) # List of prefix list IDs (for allowing access to VPC endpoints)
      description      = optional(string)       # Description of this rule
      from_port        = optional(number)       # Start port (or ICMP type number if protocol is `icmp`)
      to_port          = optional(number)       # End range port (or ICMP type number if protocol is `icmp`)
      protocol         = string                 # Protocol for the rule, If you select a protocol of "-1", you must specify a `from_port` and `to_port` equal to 0. If not `icmp`, `tcp`, `udp`, `-1`
      self             = optional(bool)         # Whether the security group itself will be added as a source to this rule.
      security_groups  = optional(list(string)) # Whether other security groups can be added as source
    })))
    sg_egress_rules = optional(list(object({
      cidr_blocks      = optional(list(string)) # List of IPv4 CIDR Blocks
      ipv6_cidr_blocks = optional(list(string)) # List of IPv6 CIDR Blocks
      prefix_list_ids  = optional(list(string)) # List of prefix list IDs (for allowing access to VPC endpoints)
      description      = optional(string)       # Description of this rule
      from_port        = optional(number)       # Start port (or ICMP type number if protocol is `icmp`)
      to_port          = optional(number)       # End range port (or ICMP type number if protocol is `icmp`)
      protocol         = string                 # Protocol for the rule, If you select a protocol of "-1", you must specify a `from_port` and `to_port` equal to 0. If not `icmp`, `tcp`, `udp`, `-1`
      self             = optional(bool)         # Whether the security group itself will be added as a source to this rule.
      security_groups  = optional(list(string)) # Whether other security groups can be added as source
    })))
  }))
}

variable "default_tags" {
  description = "Additional resource tags"
  type        = map(string)
}

variable "region" {
  description = "Region where to deploy Security Group"
  type        = string
}
