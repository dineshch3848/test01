/*
  * Security Group Module
  *
  * This module handles the creation of Security Group of an AWS Account.
  * 
*/

module "sg" {
  source          = "./modules/sg"
  security_groups = var.security_groups
  default_tags    = var.default_tags
  region          = var.region
}
