# AWS account region for first account
variable "region_first" {
  type    = string
  default = "us-west-2"
}

# AWS account region for second account
variable "region_second" {
  type    = string
  default = "us-west-2"
}

# IAM Role name in the first account
variable "name" {
  type = string
  default = "codebuild-GitOpsSecurityGroup-service-role"
}