terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=4.0.0"
    }
  }
}

# First account
provider "aws" {
  shared_credentials_files = ["credentials"]
  profile = "first"
  region  = var.region_first
}

# Second account
provider "aws" {
  shared_credentials_files = ["credentials"]
  profile = "second"
  region  = var.region_second
  alias   = "second"
}