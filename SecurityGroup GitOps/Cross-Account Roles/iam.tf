### SECOND ACCOUNT ###

resource "aws_iam_role" "role" {
  name = "assumed-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow",
        Action    = "sts:AssumeRole",
        Principal = { "AWS" : "arn:aws:iam::${data.aws_caller_identity.first.account_id}:root" }
    }]
  })
  provider = aws.second
}

resource "aws_iam_policy" "fullaccess" {
  name        = "Fullaccess"
  description = "Allows full access for all services"
  policy      = file("role_permissions_policy.json")

  provider = aws.second
}

resource "aws_iam_policy_attachment" "fullaccess" {
  name       = "Allows full access policy to role"
  roles      = ["${aws_iam_role.role.name}"]
  policy_arn = aws_iam_policy.fullaccess.arn
  provider   = aws.second
}

### FIRST ACCOUNT ###

data "aws_iam_role" "example" {
  name = var.name
}

resource "aws_iam_policy" "assumepolicy" {
  name        = "Assume-policy"
  description = "Allow assuming role"
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = "arn:aws:iam::${data.aws_caller_identity.second.account_id}:role/${aws_iam_role.role.name}"
    }]
  })
}

resource "aws_iam_role_policy_attachment" "assumepolicy" {
  role       = data.aws_iam_role.example.name
  policy_arn = aws_iam_policy.assumepolicy.arn
}