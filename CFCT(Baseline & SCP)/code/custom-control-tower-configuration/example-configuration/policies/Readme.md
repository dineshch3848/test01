
**Topics**
+ [Disallow Creation of Access Keys for the Root User](#disallow-root-access-keys)
+ [Disallow Actions as a Root User](#disallow-root-auser-actions)
+ [Restrict Region Enable/Disable Actions to a Privileged Role](#restrict-region-privileged-role)
+ [Prevent Users from Disabling AWS CloudTrail](#prevent-users-disabling-cloudTrail)
+ [Prevent Users from Disabling AWS Config or Changing Its Rules](#prevent-users-disabling-config-changing-rules)
+ [Prevent Users from Disabling Amazon CloudWatch or Altering Its Configuration](#prevent-users-disabling-cloudWatch-altering-configuration)
+ [Prevent Any VPC That Doesn't Already Have Internet Access from Getting It](#prevent-vpc-disabling-internet-access)
+ [Protect VPC Connectivity Settings from Modification](#protect-vpc-modification)
+ [Protect VPC Internet and NAT Gateway Settings from any Modifications](#protect-vpc-internet-nat-gateway-modification)
+ [Prevent Users from Deleting Amazon VPC Flow Logs](#prevent-users-deleting-vpc-flow-logs)
+ [Prevent Users from Creating Default VPC and Subnet](#prevent-users-creating-vpc-subnet)
+ [Prevent Users from Deleting S3 Buckets or Objects](#prevent-users-deleting-s3-buckets-objects)
+ [Require Encryption on All Amazon S3 Buckets in an AWS Account](#require-encryption-s3-buckets)
+ [Prevent Users from Modifying S3 Block Public Access (Account-Level)](#prevent-users-modifying-s3-public-access)
+ [Prevent Users from Deleting Glacier Vaults or Archives](#prevent-users-deleting-glacier-archives)
+ [Prevent Users from Deleting KMS Keys](#prevent-users-deleting-kms)
+ [Prevent Users from Leaving AWS Organizations](#prevent-users-leaving-org)
+ [Prevent Users from Disabling or Modifying Amazon GuardDuty Settings](#prevent-users-disabling-modifying-guardDuty)
+ [Prevent Users from Modifying Account and Billing Settings](#prevent-users-modifying-billing)
+ [Restrict the Use of the Root User in an AWS Account](#restrict-use-root-user)
+ [Prevent Creation of New IAM Users or Access Keys](#prevent-creation-iam-users-key)
+ [Prevent Creation of New IAM Users or Access Keys with an Exception for an Administrator Role](#prevent-creation-new-users-except-administrator)
+ [Prevent Modification of IAM Password Policy with an Exception for an Administrator Role](#prevent-modification-policy-except-administrator)
+ [Prevent IAM Changes to a Specified IAM Role](#prevent-iam-changes-specified-role)
+ [Prevent IAM Changes to a Specified IAM Role with the Exception of that Role](#prevent-iam-specified-except-that-role)
+ [Require Amazon EC2 Instances to Use a Specific Type](#Require-ec2-specific-type)
+ [Prevent Users from Disabling EBS Default Encryption](#prevent-users-disabling-ebs-encryption)
+ [Prevent Modifications to Specific Lambda Functions](#prevent-modification-specific-lambda)


## Disallow Creation of Access Keys for the Root User<a name="disallow-root-access-keys"></a>

Secures your AWS accounts by disallowing creation of access keys for the root user\. We recommend that you instead create access keys for the IAM users or IAM Identity Center users, which grant limited permissions to interact with your AWS account\. This is a preventive control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "GRRESTRICTROOTUSERACCESSKEYS",
            "Effect": "Deny",
            "Action": "iam:CreateAccessKey",
            "Resource": [
                "*"
            ],
            "Condition": {
                "StringLike": {
                    "aws:PrincipalArn": [
                        "arn:aws:iam::*:root"
                    ]
                }
            }
        }
    ]
}
```

## Disallow Actions as a Root User<a name="disallow-root-auser-actions"></a>

Secures your AWS accounts by disallowing account access with root user credentials, which are credentials of the account owner that allow unrestricted access to all resources in the account\. Instead, we recommend that you create IAM Identity Center users for everyday interaction with your AWS account\. This is a preventive control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following SCP\.

```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "GRRESTRICTROOTUSER",
      "Effect": "Deny",
      "Action": "*",
      "Resource": [
        "*"
      ],
      "Condition": {
        "StringLike": {
          "aws:PrincipalArn": [
            "arn:aws:iam::*:root"
          ]
        }
      }
    }
  ]
}
```

## Restrict Region Enable/Disable Actions to a Privileged Role<a name="restrict-region-privileged-role"></a>

This SCP restricts IAM principals in accounts from enabling/disabling AWS regions except if the change was being done by that specified role(This could be a common administrative IAM role created in all accounts in your organization)\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "account:EnableRegion",
                "account:DisableRegion"
            ],
            "Resource": "*",
            "Effect": "Deny",
            "Condition": {
                "ArnNotLike": {
                    "aws:PrincipalARN": "arn:aws:iam::*:role/cfct-privileged-role"
                }
            }
        }
    ]
}
```

## Prevent Users from Disabling AWS CloudTrail<a name="prevent-users-disabling-cloudTrail"></a>

This SCP prevents users or roles in any affected account from disabling a CloudTrail log, either directly as a command or through the console\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "cloudtrail:StopLogging",
                "cloudtrail:DeleteTrail",
                "cloudtrail:PutEventSelectors",                
                "cloudtrail:UpdateTrail"                
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Users from Disabling AWS Config or Changing Its Rules<a name="prevent-users-disabling-config-changing-rules"></a>

This SCP prevents users or roles in any affected account from running AWS Config operations that could disable AWS Config or alter its rules or triggers\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "config:DeleteConfigRule",
                "config:DeleteConfigurationRecorder",
                "config:DeleteDeliveryChannel",
                "config:StopConfigurationRecorder"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```



## Prevent Users from Disabling Amazon CloudWatch or Altering Its Configuration<a name="prevent-users-disabling-cloudWatch-altering-configuration"></a>

This SCP prevents users or roles in any affected account from running any of the CloudWatch commands that could delete or change your dashboards or alarms\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "cloudwatch:DeleteAlarms",
                "cloudwatch:DeleteDashboards",
                "cloudwatch:DisableAlarmActions",
                "cloudwatch:PutDashboard",
                "cloudwatch:PutMetricAlarm",
                "cloudwatch:SetAlarmState"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Any VPC That Doesn't Already Have Internet Access from Getting It<a name="prevent-vpc-disabling-internet-access"></a>

This SCP prevents users or roles in any affected account from changing the configuration of your Amazon EC2 virtual private clouds (VPCs) to grant them direct access to the internet\. It doesn't block existing direct access or any access that routes through your on-premises network environment\.

The SCP limits intenet connectivity through the following:

Prevents attaching IPv4 and IPv6 Internet Gateways (IGW) to allow direct internet access to the VPC\.
Prevents creating VPC Peering connections which might allow indirect internet access through another VPC that has it\.
Prevents modifications of Global Accelerator configuration which can allow internet access to private VPC resources directly\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:AttachInternetGateway",
                "ec2:CreateInternetGateway",
                "ec2:AttachEgressOnlyInternetGateway",
                "ec2:CreateVpcPeeringConnection",
                "ec2:AcceptVpcPeeringConnection"
            ],
            "Resource": "*",
            "Effect": "Deny"
        },
        {
            "Action": [
                "globalaccelerator:Create*",
                "globalaccelerator:Update*"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Protect VPC Connectivity Settings from Modification<a name="protect-vpc-modification"></a>

This SCP restricts IAM principals in an AWS account from changing creating, updating or deleting settings for Internet Gateways, NAT Gateways, VPC Peering, VPN Gateways, Client VPNs, Direct Connect and Global Accelerator\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:CreateNatGateway",
                "ec2:CreateInternetGateway",
                "ec2:DeleteNatGateway",
                "ec2:AttachInternetGateway",
                "ec2:DeleteInternetGateway",
                "ec2:DetachInternetGateway",
                "ec2:CreateClientVpnRoute",
                "ec2:AttachVpnGateway",
                "ec2:DisassociateClientVpnTargetNetwork",
                "ec2:DeleteClientVpnEndpoint",
                "ec2:DeleteVpcPeeringConnection",
                "ec2:AcceptVpcPeeringConnection",
                "ec2:CreateNatGateway",
                "ec2:ModifyClientVpnEndpoint",
                "ec2:CreateVpnConnectionRoute",
                "ec2:RevokeClientVpnIngress",
                "ec2:RejectVpcPeeringConnection",
                "ec2:DetachVpnGateway",
                "ec2:DeleteVpnConnectionRoute",
                "ec2:CreateClientVpnEndpoint",
                "ec2:AuthorizeClientVpnIngress",
                "ec2:DeleteVpnGateway",
                "ec2:TerminateClientVpnConnections",
                "ec2:DeleteClientVpnRoute",
                "ec2:ModifyVpcPeeringConnectionOptions",
                "ec2:CreateVpnGateway",
                "ec2:DeleteNatGateway",
                "ec2:DeleteVpnConnection",
                "ec2:CreateVpcPeeringConnection",
                "ec2:CreateVpnConnection"
            ],
            "Resource": "*",
            "Effect": "Deny"
        },
        {
            "Action": [
                "directconnect:CreatePrivateVirtualInterface",
                "directconnect:DeleteBGPPeer",
                "directconnect:DeleteLag",
                "directconnect:AssociateHostedConnection",
                "directconnect:CreateInterconnect",
                "directconnect:CreatePublicVirtualInterface",
                "directconnect:CreateLag",
                "directconnect:CreateDirectConnectGateway",
                "directconnect:AssociateVirtualInterface",
                "directconnect:AllocateConnectionOnInterconnect",
                "directconnect:AssociateConnectionWithLag",
                "directconnect:AllocatePrivateVirtualInterface",
                "directconnect:DeleteInterconnect",
                "directconnect:AllocateHostedConnection",
                "directconnect:DeleteDirectConnectGateway",
                "directconnect:DeleteVirtualInterface",
                "directconnect:DeleteDirectConnectGatewayAssociation",
                "directconnect:CreateDirectConnectGatewayAssociation",
                "directconnect:DeleteConnection",
                "directconnect:CreateBGPPeer",
                "directconnect:AllocatePublicVirtualInterface",
                "directconnect:CreateConnection"
            ],
            "Resource": "*",
            "Effect": "Deny"
        },
        {
            "Action": [
                "globalaccelerator:DeleteListener",
                "globalaccelerator:DeleteAccelerator",
                "globalaccelerator:UpdateListener",
                "globalaccelerator:UpdateAccelerator",
                "globalaccelerator:CreateEndpointGroup",
                "globalaccelerator:UpdateAcceleratorAttributes",
                "globalaccelerator:UpdateEndpointGroup",
                "globalaccelerator:CreateListener",
                "globalaccelerator:CreateAccelerator",
                "globalaccelerator:DeleteEndpointGroup"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Protect VPC Internet and NAT Gateway Settings from any Modifications<a name="protect-vpc-internet-nat-gateway-modification)"></a>

This SCP restricts IAM principals in an AWS account from changing creating, updating or deleting Internet Gateways and NAT Gateways.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:CreateNatGateway",
                "ec2:CreateInternetGateway",
                "ec2:DeleteNatGateway",
                "ec2:AttachInternetGateway",
                "ec2:DeleteInternetGateway",
                "ec2:DetachInternetGateway"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Users from Deleting Amazon VPC Flow Logs<a name="prevent-users-deleting-vpc-flow-logs"></a>

This SCP prevents users or roles in any affected account from deleting Amazon EC2 flow logs or CloudWatch log groups or log streams.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:DeleteFlowLogs",
                "logs:DeleteLogGroup",
                "logs:DeleteLogStream"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Users from Creating Default VPC and Subnet<a name="prevent-users-creating-vpc-subnet"></a>

This SCP prevents users or roles in any affected account from creating a default VPC or Subnets.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:CreateDefaultSubnet",
                "ec2:CreateDefaultVpc"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Users from Deleting S3 Buckets or Objects<a name="prevent-users-deleting-s3-buckets-objects"></a>

This SCP prevents users or roles in any affected account from deleting any S3 bucket or objects.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:DeleteBucket",
                "s3:DeleteObject",
                "s3:DeleteObjectVersion"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Require Encryption on All Amazon S3 Buckets in an AWS Account<a name="require-encryption-s3-buckets"></a>

This SCP requires that all Amazon S3 buckets use AES256 encryption in an AWS Account.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:PutObject"
            ],
            "Resource": "*",
            "Effect": "Deny",
            "Condition": {
                "StringNotEquals": {
                    "s3:x-amz-server-side-encryption": "AES256"
                }
            }
        },
        {
            "Action": [
                "s3:PutObject"
            ],
            "Resource": "*",
            "Effect": "Deny",
            "Condition": {
                "Bool": {
                    "s3:x-amz-server-side-encryption": false
                }
            }
        }
    ]
}
```

## Prevent Users from Modifying S3 Block Public Access (Account-Level)<a name="prevent-users-modifying-s3-public-access"></a>

This SCP prevents users or roles in any affected account from modifying the S3 Block Public Access Account Level Settings\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:PutAccountPublicAccessBlock"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Users from Deleting Glacier Vaults or Archives<a name="prevent-users-deleting-glacier-archives"></a>

This SCP prevents users or roles in any affected account from deleting any S3 Glacier vaults or archives\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "glacier:DeleteArchive",
                "glacier:DeleteVault"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Users from Deleting KMS Keys<a name="prevent-users-deleting-kms"></a>

This SCP prevents users or roles in any affected account from deleting KMS keys, either directly as a command or through the console\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "kms:ScheduleKeyDeletion",
                "kms:Delete*"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Users from Leaving AWS Organizations<a name="prevent-users-leaving-org"></a>

This SCP prevents users or roles in any affected account from leaving AWS Organizations, either directly as a command or through the console\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "organizations:LeaveOrganization"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Users from Disabling or Modifying Amazon GuardDuty Settings<a name="prevent-users-disabling-modifying-guardDuty"></a>

This SCP prevents users or roles in any affected account from disabling or modifying Amazon GuardDuty settings, either directly as a command or through the console\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "guardduty:DeleteDetector",
                "guardduty:DeleteInvitations",
                "guardduty:DeleteIPSet",
                "guardduty:DeleteMembers",
                "guardduty:DeleteThreatIntelSet",
                "guardduty:DisassociateFromMasterAccount",
                "guardduty:DisassociateMembers",
                "guardduty:StopMonitoringMembers",
                "guardduty:UpdateDetector"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Users from Modifying Account and Billing Settings<a name="prevent-users-modifying-billing"></a>

This SCP prevents users or roles in any affected account from modifying the account and billing settings, either directly as a command or through the console\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "aws-portal:ModifyAccount",
                "aws-portal:ModifyBilling",
                "aws-portal:ModifyPaymentMethods"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Restrict the Use of the Root User in an AWS Account<a name="restrict-use-root-user"></a>

This SCP prevents restricts the root user in an AWS account from taking any action, either directly as a command or through the console\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "*",
            "Resource": "*",
            "Effect": "Deny",
            "Condition": {
                "StringLike": {
                    "aws:PrincipalArn": [
                        "arn:aws:iam::*:root"
                    ]
                }
            }
        }
    ]
}
```

## Prevent Creation of New IAM Users or Access Keys<a name="prevent-creation-iam-users-key"></a>

This SCP restricts IAM principals from creating new IAM users or IAM Access Keys in an AWS account\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "iam:CreateUser",
                "iam:CreateAccessKey"
            ],
            "Resource": [
                "*"
            ],
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Creation of New IAM Users or Access Keys with an Exception for an Administrator Role<a name="prevent-creation-new-users-except-administrator"></a>

This SCP restricts IAM principals from creating new IAM users or IAM Access Keys in an AWS account with an exception for a specified Administrator IAM role.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "iam:CreateUser",
                "iam:CreateAccessKey"
            ],
            "Resource": [
                "*"
            ],
            "Effect": "Deny",
            "Condition": {
                "StringNotEquals": {
                    "aws:PrincipalARN": "arn:aws:iam::*:role/cfct-admin-role"
                }
            }
        }
    ]
}
```

## Prevent Modification of IAM Password Policy with an Exception for an Administrator Role<a name="prevent-modification-policy-except-administrator"></a>

This SCP restricts IAM principals from modifying existing IAM password policies in an AWS account with an exception for a specified Administrator IAM role.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "iam:DeleteAccountPasswordPolicy",
                "iam:UpdateAccountPasswordPolicy"
            ],
            "Resource": [
                "*"
            ],
            "Effect": "Deny",
            "Condition": {
                "StringNotEquals": {
                    "aws:PrincipalARN": "arn:aws:iam::*:role/cfct-admin-role"
                }
            }
        }
    ]
}
```

## Prevent IAM Changes to a Specified IAM Role<a name="prevent-iam-changes-specified-role"></a>

This SCP restricts IAM principals in accounts from making changes to an IAM role created in an AWS account (This could be a common administrative IAM role created in all accounts in your organization)\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "iam:AttachRolePolicy",
                "iam:DeleteRole",
                "iam:DeleteRolePermissionsBoundary",
                "iam:DeleteRolePolicy",
                "iam:DetachRolePolicy",
                "iam:PutRolePermissionsBoundary",
                "iam:PutRolePolicy",
                "iam:UpdateAssumeRolePolicy",
                "iam:UpdateRole",
                "iam:UpdateRoleDescription"
            ],
            "Resource": [
                "arn:aws:iam::*:role/cfct-admin-role"
            ],
            "Effect": "Deny"
        }
    ]
}
```

## Prevent IAM Changes to a Specified IAM Role with the Exception of that Role<a name="prevent-iam-specified-except-that-role"></a>

This SCP restricts IAM principals in accounts from making changes to an IAM role created in an AWS account except if the change was being done by that specified role (This could be a common administrative IAM role created in all accounts in your organization).

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "iam:AttachRolePolicy",
                "iam:DeleteRole",
                "iam:DeleteRolePermissionsBoundary",
                "iam:DeleteRolePolicy",
                "iam:DetachRolePolicy",
                "iam:PutRolePermissionsBoundary",
                "iam:PutRolePolicy",
                "iam:UpdateAssumeRolePolicy",
                "iam:UpdateRole",
                "iam:UpdateRoleDescription"
            ],
            "Resource": [
                "arn:aws:iam::*:role/cfct-admin-role"
            ],
            "Effect": "Deny",
            "Condition": {
                "StringNotEquals": {
                    "aws:PrincipalARN": "arn:aws:iam::*:role/cfct-admin-role"
                }
            }
        }
    ]
}
```

## Require Amazon EC2 Instances to Use a Specific Type<a name="Require-ec2-specific-type"></a>

This SCP prevents the launch of any EC2 instance type that is not whitelisted by the policy (default: t3.micro).

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:RunInstances"
            ],
            "Resource": "*",
            "Effect": "Deny",
            "Condition": {
                "StringNotEquals": {
                    "ec2:InstanceType": "t3.micro"
                }
            }
        }
    ]
}
```

## Prevent Users from Disabling EBS Default Encryption<a name="prevent-users-disabling-ebs-encryption"></a>

This SCP prevents users or roles in any affected account from disabling ebs default encryption\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:DisableEbsEncryptionByDefault"
            ],
            "Resource": "*",
            "Effect": "Deny"
        }
    ]
}
```

## Prevent Modifications to Specific Lambda Functions<a name="prevent-modification-specific-lambda"></a>

This SCP restricts IAM principals in accounts from making changes to specific Lambda Functions with the exception of a specific IAM role (This could be a common administrative IAM role created in all accounts in your organization)\.

The artifact for this control is the following SCP\.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "lambda:AddPermission",
                "lambda:CreateEventSourceMapping",
                "lambda:CreateFunction",
                "lambda:DeleteEventSourceMapping",
                "lambda:DeleteFunction",
                "lambda:DeleteFunctionConcurrency",
                "lambda:PutFunctionConcurrency",
                "lambda:RemovePermission",
                "lambda:UpdateEventSourceMapping",
                "lambda:UpdateFunctionCode",
                "lambda:UpdateFunctionConfiguration"
            ],
            "Resource": [
                "arn:aws:lambda:*:*:function:cfct-test-lambda"
            ],
            "Effect": "Deny",
            "Condition": {
                "ArnNotLike": {
                    "aws:PrincipalARN": "arn:aws:iam::*:role/cfct-admin-role"
                }
            }
        }
    ]
}
```


