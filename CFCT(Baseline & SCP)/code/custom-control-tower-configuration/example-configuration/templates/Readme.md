
**Topics**
+ [Detect Whether Encryption is Enabled for Amazon EBS Volumes Attached to Amazon EC2 Instances](#ebs-enable-encryption)
+ [Detect Whether Unrestricted Incoming TCP Traffic is Allowed](#rdp-disallow-internet)
+ [Detect Whether Unrestricted Internet Connection Through SSH is Allowed](#ssh-disallow-internet)
+ [Detect Whether MFA for the Root User is Enabled](#enable-root-mfa)
+ [Detect Whether Public Read Access to Amazon S3 Buckets is Allowed](#s3-disallow-public-read)
+ [Detect Whether Public Write Access to Amazon S3 Buckets is Allowed](#s3-disallow-public-write)
+ [Detect Whether Amazon EBS Volumes are Attached to Amazon EC2 Instances](#disallow-unattached-ebs)
+ [Detect Whether Amazon EBS Optimization is Enabled for Amazon EC2 Instances](#disallow-not-ebs-optimized)
+ [Detect Whether Public Access to Amazon RDS Database Instances is Enabled](#disallow-rds-public-access)
+ [Detect Whether Public Access to Amazon RDS Database Snapshots is Enabled](#disallow-rds-snapshot-public-access)
+ [Detect Whether Storage Encryption is Enabled for Amazon RDS Database Instances](#disallow-rds-storage-unencrypted)
+ [Detect whether an account has AWS CloudTrail or CloudTrail Lake enabled](#ensure-cloudtrail-enabled-recommended)
+ [Required Tags](#required-tags)
+ [Security Groups Unrestricted Common Ports Check](#security-groups-unrestricted-common-ports)
+ [No EC2 Instances in Public Subnets Check](#ec2-instances-public-subnets)
+ [Security Groups Do Not Allow All Ports Check](#security-groups-do-not-allow-all-ports)
+ [EC2 Desired Instance Type Check](#ec2-desired-instances-type)
+ [EC2 Instances Managed by Systems Manager (SSM) Check](#ec2-managed-systems-manager)
+ [EC2 Instances Detailed Monitoring Enabled](#ec2-desired-detailed-monitoring)
+ [Unattached Elastic IPs (EIP) Check](#unattached-elastic-ip-check)
+ [EC2 Instance Profile Attached](#ec2-instances-profile-attached)

*IAM*
+ [IAM Password Policy Settings Check](#iam-password-policy-settings-ports)
+ [Root Account MFA Enabled Check](#root-account-mfa-enabled)
+ [IAM Users MFA Enabled Check](#iam-users-mfa-enabled)
+ [IAM Policies don't allow Admin Access Check](#iam-policies-restrict-admin-access)
+ [Root Access Keys Does Not Exist Check](#root-access-keys-does-not-exist)
+ [IAM Users Unused Credentials Check](#iam-users-unused-credentials)
+ [Mfa Enabled for IAM Console Access](#mfa-enabled-iam-console-access)

*S3*
+ [S3 Bucket Logging Enabled Check](#s3-bucket-logging-enabled-ports)
+ [S3 Bucket Public Read Disabled Check](#s3-bucket-public-read-disabled)
+ [S3 Bucket Public Write Disabled Check](#s3-bucket-public-write-disabled)
+ [S3 Bucket Server Side Encryption Enabled Check](#s3-bucket-server-side-encryption-enabled)
+ [S3 Bucket Versioning Enabled Check](#s3-bucket-versioning-enabled)
+ [S3 Bucket Default Encryption with KMS Enabled](#s3-bucket-Default-encryption-kms-enabled)
+ [CloudTrail Data Events are Enabled for S3 Buckets Check](#cloudTrail-data-events-enabled-s3)
+ [S3 Block Public Access Enabled (Account-Level)](#s3-block-public-access-enabled)
+ [S3 Lifecycle Policy Check](#s3-lifecycle-policy)
+ [S3 Version Lifecycle Policy Check](#s3-version-lifecycle-policy)

*RDS*

+ [RDS Storage Encrypted Check](#rds-storage-encrypted)
+ [RDS Multi-AZ HA Enabled Check](#rds-multi-az-ha-enabled)
+ [No RDS Instances in Public Subnets Check](#rds-restrict-public-subnets)
+ [RDS Enhanced Monitoring Enabled](#rds-enhanced-monitoring-enabled)
+ [RDS Backup Enabled Check](#rds-backup-enabled)
+ [RDS Snapshot Encrypted Check](#rds-snapshot-encrypted)
+ [RDS Cluster Deletion Protection Enabled](#rds-cluster-deletion-protection-enabled)
+ [RDS Instance Deletion Protection Enabled](#rds-Instance-deletion-protection-enabled)

*Other Services*
+ [CloudTrail Multi-Region Trail Enabled Check](#cloudTrail-multi-region-enabled)
+ [Amazon GuardDuty Enabled and Centralized (optional) Check](#guardDuty-enabled-centralized)
+ [GuardDuty Untreated Findings Check](#guardDuty-untreated-findings)
+ [VPC Flow Logs Enabled Check](#vpc-flow-logs-enabled)
+ [NACL Does Not Allow Unrestricted SSH or RDP Check](#NACL-does-not-allow-unrestricted-ssh-rdp)
+ [Security Hub Enabled](#security-hub-enabled)
+ [CloudFormation Stack Notification Check](#cloudFormation-stack-notification)
+ [Amazon Elasticsearch Encrypted at Rest](#elasticsearch-encrypted-at-rest)
+ [Secrets Manager Rotation Enabled Check](#secrets-manager-rotation-enabled)
+ [Account Part of AWS Organizations Check](#account-part-aws-organizations)
+ [ECR Private Image Scanning Enabled](#ecr-private-image-scanning)
+ [ECR Private Tag Immutability Enabled](#ecr-private-Immutability-enabled)



## Detect Whether Encryption is Enabled for Amazon EBS Volumes Attached to Amazon EC2 Instances<a name="ebs-enable-encryption"></a>

This control detects whether the Amazon EBS volumes attached to an Amazon EC2 instance are encrypted\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control isn't enabled on any OUs\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to check for encryption of all storage volumes attached to compute
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
    #Default: encrypted-volumes-by-parameter-value
Resources:
  CheckForEncryptedVolumes:
    Type: AWS::Config::ConfigRule
    Properties:
      #ConfigRuleName: !Sub ${ConfigRuleName}
      ConfigRuleName: "encrypted-volumes"
      Description: Checks whether EBS volumes that are in an attached state are encrypted.
      Source:
        Owner: AWS
        SourceIdentifier: ENCRYPTED_VOLUMES
      Scope:
        ComplianceResourceTypes:
          - AWS::EC2::Volume
```

## Detect Whether Unrestricted Incoming TCP Traffic is Allowed<a name="rdp-disallow-internet"></a>

This control helps reduce a server's exposure to risk by detecting whether unrestricted incoming TCP traffic is allowed\. It detects whether internet connections are enabled to Amazon EC2 instances through services such as Remote Desktop Protocol \(RDP\)\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to check whether security groups that are in use disallow unrestricted incoming TCP traffic to the specified ports.
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
  blockedPort1:
    Type: String
    Default: '20'
    Description: Blocked TCP port number.
  blockedPort2:
    Type: String
    Default: '21'
    Description: Blocked TCP port number.
  blockedPort3:
    Type: String
    Default: '3389'
    Description: Blocked TCP port number.
  blockedPort4:
    Type: String
    Default: '3306'
    Description: Blocked TCP port number.
  blockedPort5:
    Type: String
    Default: '4333'
    Description: Blocked TCP port number.
Conditions:
  blockedPort1:
    Fn::Not:
    - Fn::Equals:
      - ''
      - Ref: blockedPort1
  blockedPort2:
    Fn::Not:
    - Fn::Equals:
      - ''
      - Ref: blockedPort2
  blockedPort3:
    Fn::Not:
    - Fn::Equals:
      - ''
      - Ref: blockedPort3
  blockedPort4:
    Fn::Not:
    - Fn::Equals:
      - ''
      - Ref: blockedPort4
  blockedPort5:
    Fn::Not:
    - Fn::Equals:
      - ''
      - Ref: blockedPort5
Resources:
  CheckForRestrictedCommonPortsPolicy:
    Type: AWS::Config::ConfigRule
    Properties:
      ConfigRuleName: !Sub ${ConfigRuleName}
      Description: Checks whether security groups that are in use disallow unrestricted incoming TCP traffic to the specified ports.
      InputParameters:
        blockedPort1:
          Fn::If:
          - blockedPort1
          - Ref: blockedPort1
          - Ref: AWS::NoValue
        blockedPort2:
          Fn::If:
          - blockedPort2
          - Ref: blockedPort2
          - Ref: AWS::NoValue
        blockedPort3:
          Fn::If:
          - blockedPort3
          - Ref: blockedPort3
          - Ref: AWS::NoValue
        blockedPort4:
          Fn::If:
          - blockedPort4
          - Ref: blockedPort4
          - Ref: AWS::NoValue
        blockedPort5:
          Fn::If:
          - blockedPort5
          - Ref: blockedPort5
          - Ref: AWS::NoValue
      Scope:
        ComplianceResourceTypes:
        - AWS::EC2::SecurityGroup
      Source:
        Owner: AWS
        SourceIdentifier: RESTRICTED_INCOMING_TRAFFIC
```

## Detect Whether Unrestricted Internet Connection Through SSH is Allowed<a name="ssh-disallow-internet"></a>

This control detects whether internet connections are allowed through remote services such as the Secure Shell \(SSH\) protocol\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to check whether security groups that are in use disallow SSH
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
    #Default: restricted-ssh-by-parameter-value
Resources:
  CheckForRestrictedSshPolicy:
    Type: AWS::Config::ConfigRule
    Properties:
      #ConfigRuleName: !Sub ${ConfigRuleName}
      ConfigRuleName: "restricted-ssh"
      Description: Checks whether security groups that are in use disallow unrestricted incoming SSH traffic.
      Scope:
        ComplianceResourceTypes:
        - AWS::EC2::SecurityGroup
      Source:
        Owner: AWS
        SourceIdentifier: INCOMING_SSH_DISABLED
```

## Detect Whether MFA for the Root User is Enabled<a name="enable-root-mfa"></a>

This control detects whether multi\-factor authentication \(MFA\) is enabled for the root user\. MFA reduces vulnerability risks from weak authentication by requiring an additional authentication code after the user name and password are successful\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to require MFA for root access to accounts
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
  MaximumExecutionFrequency:
    Type: String
    Default: 24hours
    Description: The frequency that you want AWS Config to run evaluations for the rule.
    AllowedValues:
    - 1hour
    - 3hours
    - 6hours
    - 12hours
    - 24hours
Mappings:
  Settings:
    FrequencyMap:
      1hour   : One_Hour
      3hours  : Three_Hours
      6hours  : Six_Hours
      12hours : Twelve_Hours
      24hours : TwentyFour_Hours
Resources:
  CheckForRootMfa:
    Type: AWS::Config::ConfigRule
    Properties:
      ConfigRuleName: !Sub ${ConfigRuleName}
      Description: Checks whether the root user of your AWS account requires multi-factor authentication for console sign-in.
      Source:
        Owner: AWS
        SourceIdentifier: ROOT_ACCOUNT_MFA_ENABLED
      MaximumExecutionFrequency:
        !FindInMap
          - Settings
          - FrequencyMap
          - !Ref MaximumExecutionFrequency
```

## Detect Whether Public Read Access to Amazon S3 Buckets is Allowed<a name="s3-disallow-public-read"></a>

This control detects whether public read access is allowed to Amazon S3 buckets\. It helps you maintain secure access to data stored in the buckets\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to check that your S3 buckets do not allow public access
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
Resources:
  CheckForS3PublicRead:
    Type: AWS::Config::ConfigRule
    Properties:
      ConfigRuleName: !Sub ${ConfigRuleName}
      Description: Checks that your S3 buckets do not allow public read access. If an S3 bucket policy or bucket ACL allows public read access, the bucket is noncompliant.
      Source:
        Owner: AWS
        SourceIdentifier: S3_BUCKET_PUBLIC_READ_PROHIBITED
      Scope:
        ComplianceResourceTypes:
          - AWS::S3::Bucket
```

## Detect Whether Public Write Access to Amazon S3 Buckets is Allowed<a name="s3-disallow-public-write"></a>

This control detects whether public write access is allowed to Amazon S3 buckets\. It helps you maintain secure access to data stored in the buckets\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to check that your S3 buckets do not allow public access
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
Resources:
  CheckForS3PublicWrite:
    Type: AWS::Config::ConfigRule
    Properties:
      ConfigRuleName: !Sub ${ConfigRuleName}
      Description: Checks that your S3 buckets do not allow public write access. If an S3 bucket policy or bucket ACL allows public write access, the bucket is noncompliant.
      Source:
        Owner: AWS
        SourceIdentifier: S3_BUCKET_PUBLIC_WRITE_PROHIBITED
      Scope:
        ComplianceResourceTypes:
          - AWS::S3::Bucket
```

## Detect Whether Amazon EBS Volumes are Attached to Amazon EC2 Instances<a name="disallow-unattached-ebs"></a>

This control detects whether an Amazon EBS volume device persists independently from an Amazon EC2 instance\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to check whether EBS volumes are attached to EC2 instances
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
    #Default: ec2-volume-inuse-check-by-parameter
  deleteOnTermination:
    Type: 'String'
    Default: 'None'
    Description: 'Check for Delete on termination'
Conditions:
  deleteOnTermination:
    Fn::Not:
    - Fn::Equals:
      - 'None'
      - Ref: deleteOnTermination
Resources:
  CheckForEc2VolumesInUse:
    Type: AWS::Config::ConfigRule
    Properties:
      #ConfigRuleName: !Sub ${ConfigRuleName}
      ConfigRuleName: "ec2-volume-inuse-check"
      Description: Checks whether EBS volumes are attached to EC2 instances
      InputParameters:
        deleteOnTermination:
          Fn::If:
            - deleteOnTermination
            - Ref: deleteOnTermination
            - Ref: AWS::NoValue
      Source:
        Owner: AWS
        SourceIdentifier: EC2_VOLUME_INUSE_CHECK
      Scope:
        ComplianceResourceTypes:
          - AWS::EC2::Volume
```

## Detect Whether Amazon EBS Optimization is Enabled for Amazon EC2 Instances<a name="disallow-not-ebs-optimized"></a>

Detects whether Amazon EC2 instances are launched without an Amazon EBS volume that is optimized for performance\. Amazon EBS\-optimized volumes minimize contention between Amazon EBS I/O and other traffic from your instance\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to check whether EBS optimization is enabled for your EC2 instances that can be EBS-optimized
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
Resources:
  CheckForEbsOptimizedInstance:
    Type: AWS::Config::ConfigRule
    Properties:
      ConfigRuleName: !Sub ${ConfigRuleName}
      Description: Checks whether EBS optimization is enabled for your EC2 instances that can be EBS-optimized
      Source:
        Owner: AWS
        SourceIdentifier: EBS_OPTIMIZED_INSTANCE
      Scope:
        ComplianceResourceTypes:
          - AWS::EC2::Instance
```

## Detect Whether Public Access to Amazon RDS Database Instances is Enabled<a name="disallow-rds-public-access"></a>

Detects whether your Amazon RDS database instances allow public access\. You can secure your Amazon RDS database instances by disallowing public access\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to check whether Amazon RDS instances are not publicly accessible.
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
Resources:
  CheckForRdsPublicAccess:
    Type: AWS::Config::ConfigRule
    Properties:
      ConfigRuleName: !Sub ${ConfigRuleName}
      Description: Checks whether the Amazon Relational Database Service (RDS) instances are not publicly accessible. The rule is non-compliant if the publiclyAccessible field is true in the instance configuration item.
      Source:
        Owner: AWS
        SourceIdentifier: RDS_INSTANCE_PUBLIC_ACCESS_CHECK
      Scope:
        ComplianceResourceTypes:
          - AWS::RDS::DBInstance
```

## Detect Whether Public Access to Amazon RDS Database Snapshots is Enabled<a name="disallow-rds-snapshot-public-access"></a>

Detects whether your Amazon RDS database snapshots have public access enabled\. You can protect your information by disabling public access\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Checks if Amazon Relational Database Service (Amazon RDS) snapshots are public.
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
Resources:
  CheckForRdsStorageEncryption:
    Type: AWS::Config::ConfigRule
    Properties:
      ConfigRuleName: !Sub ${ConfigRuleName}
      Description: Checks if Amazon Relational Database Service (Amazon RDS) snapshots are public. The rule is non-compliant if any existing and new Amazon RDS snapshots are public.
      Source:
        Owner: AWS
        SourceIdentifier: RDS_SNAPSHOTS_PUBLIC_PROHIBITED
      Scope:
        ComplianceResourceTypes:
          - AWS::RDS::DBSnapshot
```

## Detect Whether Storage Encryption is Enabled for Amazon RDS Database Instances<a name="disallow-rds-storage-unencrypted"></a>

Detects Amazon RDS database instances that are not encrypted at rest\. You can secure your Amazon RDS database instances at rest by encrypting the underlying storage for database instances and their automated backups, Read Replicas, and snapshots\. This control does not change the status of the account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
Description: Configure AWS Config rules to check whether storage encryption is enabled for your RDS DB instances
Parameters:
  ConfigRuleName:
    Type: 'String'
    Description: 'Name for the Config rule'
Resources:
  CheckForRdsStorageEncryption:
    Type: AWS::Config::ConfigRule
    Properties:
      ConfigRuleName: !Sub ${ConfigRuleName}
      Description: Checks whether storage encryption is enabled for your RDS DB instances.
      Source:
        Owner: AWS
        SourceIdentifier: RDS_STORAGE_ENCRYPTED
      Scope:
        ComplianceResourceTypes:
          - AWS::RDS::DBInstance
```

## Detect whether an account has AWS CloudTrail or CloudTrail Lake enabled<a name="ensure-cloudtrail-enabled-recommended"></a>

This control detects whether an account has AWS CloudTrail or CloudTrail Lake enabled\. The rule is NON\_COMPLIANT if either CloudTrail or CloudTrail Lake is not enabled in an account\. This is a detective control with strongly recommended guidance\. By default, this control is not enabled on any OUs\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: 2010-09-09
   Description: Configure AWS Config rules to detect whether an account has AWS CloudTrail or CloudTrail Lake enabled.
   
   Parameters:
     ConfigRuleName:
       Type: 'String'
       Description: 'Name for the Config rule'
   
   Resources:
     CheckForCloudtrailEnabled:
       Type: AWS::Config::ConfigRule
       Properties:
         ConfigRuleName: !Sub ${ConfigRuleName}
         Description: Detects whether an account has AWS CloudTrail or CloudTrail Lake enabled. The rule is NON_COMPLIANT if either CloudTrail or CloudTrail Lake is not enabled in an account.
         Source:
           Owner: AWS
           SourceIdentifier: CLOUD_TRAIL_ENABLED
```


## Required Tags<a name="required-tags"></a>

A Config rule that checks whether your resources have the tags that you specify. For example, you can check whether your EC2 instances have the 'CostCenter' tag. Separate multiple values with commas.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "required-tags"
      Scope:
        ComplianceResourceTypes:
          - "AWS::ACM::Certificate"
          - "AWS::AutoScaling::AutoScalingGroup"
          - "AWS::CloudFormation::Stack"
          - "AWS::CodeBuild::Project"
          - "AWS::DynamoDB::Table"
          - "AWS::EC2::CustomerGateway"
          - "AWS::EC2::Instance"
          - "AWS::EC2::InternetGateway"
          - "AWS::EC2::NetworkAcl"
          - "AWS::EC2::NetworkInterface"
          - "AWS::EC2::RouteTable"
          - "AWS::EC2::SecurityGroup"
          - "AWS::EC2::Subnet"
          - "AWS::EC2::Volume"
          - "AWS::EC2::VPC"
          - "AWS::EC2::VPNConnection"
          - "AWS::EC2::VPNGateway"
          - "AWS::ElasticLoadBalancing::LoadBalancer"
          - "AWS::ElasticLoadBalancingV2::LoadBalancer"
          - "AWS::RDS::DBInstance"
          - "AWS::RDS::DBSecurityGroup"
          - "AWS::RDS::DBSnapshot"
          - "AWS::RDS::DBSubnetGroup"
          - "AWS::RDS::EventSubscription"
          - "AWS::Redshift::Cluster"
          - "AWS::Redshift::ClusterParameterGroup"
          - "AWS::Redshift::ClusterSecurityGroup"
          - "AWS::Redshift::ClusterSnapshot"
          - "AWS::Redshift::ClusterSubnetGroup"
          - "AWS::S3::Bucket"
      Description: "A Config rule that checks whether your resources have the tags that you specify. For example, you can check whether your EC2 instances have the 'CostCenter' tag. Separate multiple values with commas."
      InputParameters:
        tag1Key: "Name"
        tag1Value: "cfct"
        tag2Key: "Org_name"
        tag2Value: "cfct"
      Source:
        Owner: "AWS"
        SourceIdentifier: "REQUIRED_TAGS"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Security Groups Unrestricted Common Ports Check<a name="security-groups-unrestricted-common-ports"></a>

A Config rule that checks whether security groups in use do not allow restricted incoming TCP traffic to the specified ports. This rule applies only to IPv4.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "restricted-common-ports"
      Scope:
        ComplianceResourceTypes:
          - "AWS::EC2::SecurityGroup"
      Description: "A Config rule that checks whether security groups in use do not allow restricted incoming TCP traffic to the specified ports. This rule applies only to IPv4."
      InputParameters:
        blockedPort1: "20"
        blockedPort2: "21"
        blockedPort3: "3389"
        blockedPort4: "3306"
        blockedPort5: "4333"
      Source:
        Owner: "AWS"
        SourceIdentifier: "RESTRICTED_INCOMING_TRAFFIC"
Parameters: {}
Metadata: {}
Conditions: {}
```

## No EC2 Instances in Public Subnets Check<a name="ec2-instances-public-subnets"></a>

A Config rule that checks that no EC2 Instances are in Public Subnet.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  CustomConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "ec2_vpc_public_subnet"
      Scope:
        ComplianceResourceTypes:
          - "AWS::EC2::Instance"
      Description: "A Config rule that checks that no EC2 Instances are in Public Subnet."
      Source:
        Owner: "CUSTOM_LAMBDA"
        SourceIdentifier:
          Fn::GetAtt:
            - "LambdaFunctionCustomConfigRule"
            - "Arn"
        SourceDetails:
          - EventSource: "aws.config"
            MessageType: "ConfigurationItemChangeNotification"
          - EventSource: "aws.config"
            MessageType: "OversizedConfigurationItemChangeNotification"
    DependsOn: "LambdaInvokePermissionsCustomConfigRule"
  LambdaInvokePermissionsCustomConfigRule:
    Type: "AWS::Lambda::Permission"
    Properties:
      FunctionName:
        Fn::GetAtt:
          - "LambdaFunctionCustomConfigRule"
          - "Arn"
      Action: "lambda:InvokeFunction"
      Principal: "config.amazonaws.com"
  LambdaFunctionCustomConfigRule:
    Type: "AWS::Lambda::Function"
    Properties:
      FunctionName: "LambdaForec2_vpc_public_subnet"
      Handler: "index.lambda_handler"
      Role:
        Fn::GetAtt:
          - "LambdaIamRoleCustomConfigRule"
          - "Arn"
      Runtime: "python3.9"
      Code:
        ZipFile:
          Fn::Join:
            - "\n"
            -
              - ""
              - "#"
              - "# This file made available under CC0 1.0 Universal (https://creativecommons.org/publicdomain/zero/1.0/legalcode)"
              - "#"
              - "# Description: Check that no EC2 Instances are in Public Subnet"
              - "#"
              - "# Trigger Type: Change Triggered"
              - "# Scope of Changes: EC2:Instance"
              - "# Accepted Parameters: None"
              - "# Your Lambda function execution role will need to have a policy that provides the appropriate"
              - "# permissions.  Here is a policy that you can consider.  You should validate this for your own"
              - "# environment"
              - "#{"
              - "#    \"Version\": \"2012-10-17\","
              - "#    \"Statement\": ["
              - "#        {"
              - "#            \"Effect\": \"Allow\","
              - "#            \"Action\": ["
              - "#                \"logs:CreateLogGroup\","
              - "#                \"logs:CreateLogStream\","
              - "#                \"logs:PutLogEvents\""
              - "#            ],"
              - "#            \"Resource\": \"arn:aws:logs:*:*:*\""
              - "#        },"
              - "#        {"
              - "#            \"Effect\": \"Allow\","
              - "#            \"Action\": ["
              - "#                \"config:PutEvaluations\","
              - "#                \"ec2:DescribeRouteTables\""
              - "#            ],"
              - "#            \"Resource\": \"*\""
              - "#        }"
              - "#    ]"
              - "#}"
              - "#"
              - ""
              - "import boto3"
              - "import botocore"
              - "import json"
              - "import logging"
              - ""
              - "log = logging.getLogger()"
              - "log.setLevel(logging.INFO)"
              - ""
              - "def evaluate_compliance(configuration_item):"
              - "    subnet_id   = configuration_item[\"configuration\"][\"subnetId\"]"
              - "    vpc_id      = configuration_item[\"configuration\"][\"vpcId\"]"
              - "    client      = boto3.client(\"ec2\");"
              - ""
              - "    response    = client.describe_route_tables()"
              - ""
              - "    # If the subnet is explicitly associated to a route table, check if there"
              - "    # is a public route. If no explicit association exists, check if the main"
              - "    # route table has a public route."
              - ""
              - "    private = True"
              - "    mainTableIsPublic = False"
              - "    noExplicitAssociationFound = True"
              - "    explicitAssocationIsPublic = False"
              - ""
              - "    for i in response['RouteTables']:"
              - "        if i['VpcId'] == vpc_id:"
              - "            for j in i['Associations']:"
              - "                if j['Main'] == True:"
              - "                    for k in i['Routes']:"
              - "                        if k['DestinationCidrBlock'] == '0.0.0.0/0' or k['GatewayId'].startswith('igw-'):"
              - "                            mainTableIsPublic = True"
              - "                else:"
              - "                    if j['SubnetId'] == subnet_id:"
              - "                        noExplicitAssociationFound = False"
              - "                        for k in i['Routes']:"
              - "                            if k['DestinationCidrBlock'] == '0.0.0.0/0' or k['GatewayId'].startswith('igw-'):"
              - "                                explicitAssocationIsPublic = True"
              - ""
              - "    if (mainTableIsPublic and noExplicitAssociationFound) or explicitAssocationIsPublic:"
              - "        private = False"
              - ""
              - "    if private:"
              - "        return {"
              - "            \"compliance_type\": \"COMPLIANT\","
              - "            \"annotation\": 'Its in private subnet'"
              - "        }"
              - "    else:"
              - "        return {"
              - "            \"compliance_type\" : \"NON_COMPLIANT\","
              - "            \"annotation\" : 'Not in private subnet'"
              - "        }"
              - ""
              - "def lambda_handler(event, context):"
              - "    log.debug('Event %s', event)"
              - "    invoking_event      = json.loads(event['invokingEvent'])"
              - "    configuration_item  = invoking_event[\"configurationItem\"]"
              - "    evaluation          = evaluate_compliance(configuration_item)"
              - "    config              = boto3.client('config')"
              - ""
              - "    response = config.put_evaluations("
              - "       Evaluations=["
              - "           {"
              - "               'ComplianceResourceType':    invoking_event['configurationItem']['resourceType'],"
              - "               'ComplianceResourceId':      invoking_event['configurationItem']['resourceId'],"
              - "               'ComplianceType':            evaluation[\"compliance_type\"],"
              - "               \"Annotation\":                evaluation[\"annotation\"],"
              - "               'OrderingTimestamp':         invoking_event['configurationItem']['configurationItemCaptureTime']"
              - "           },"
              - "       ],"
              - "       ResultToken=event['resultToken'])"
              - ""
      Timeout: 300
    DependsOn: "LambdaIamRoleCustomConfigRule"
  LambdaIamRoleCustomConfigRule:
    Type: "AWS::IAM::Role"
    Properties:
      RoleName: "IAMRoleForec2_vpc_public_subnetnrO"
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com"
            Action:
              - "sts:AssumeRole"
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
        - "arn:aws:iam::aws:policy/service-role/AWSConfigRulesExecutionRole"
        - "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Security Groups Do Not Allow All Ports Check<a name="security-groups-do-not-allow-all-ports"></a>

A Config rule that checks that security groups do not have an inbound rule with port range of "All"\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  CustomConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "ec2_security_group_port_range_all_prohibited"
      Scope:
        ComplianceResourceTypes:
          - "AWS::EC2::SecurityGroup"
      Description: "A Config rule that checks that security groups do not have an inbound rule with port range of \"All\"."
      Source:
        Owner: "CUSTOM_LAMBDA"
        SourceIdentifier:
          Fn::GetAtt:
            - "LambdaFunctionCustomConfigRule"
            - "Arn"
        SourceDetails:
          - EventSource: "aws.config"
            MessageType: "ConfigurationItemChangeNotification"
          - EventSource: "aws.config"
            MessageType: "OversizedConfigurationItemChangeNotification"
    DependsOn: "LambdaInvokePermissionsCustomConfigRule"
  LambdaInvokePermissionsCustomConfigRule:
    Type: "AWS::Lambda::Permission"
    Properties:
      FunctionName:
        Fn::GetAtt:
          - "LambdaFunctionCustomConfigRule"
          - "Arn"
      Action: "lambda:InvokeFunction"
      Principal: "config.amazonaws.com"
  LambdaFunctionCustomConfigRule:
    Type: "AWS::Lambda::Function"
    Properties:
      FunctionName: "LambdaForec2_security_group_port_range_all_prohibited"
      Handler: "index.lambda_handler"
      Role:
        Fn::GetAtt:
          - "LambdaIamRoleCustomConfigRule"
          - "Arn"
      Runtime: "python3.9"
      Code:
        ZipFile:
          Fn::Join:
            - "\n"
            -
              - ""
              - "# This file made available under CC0 1.0 Universal (https://creativecommons.org/publicdomain/zero/1.0/legalcode)"
              - "#"
              - "# Description: Check that security groups do not have an inbound rule"
              - "#              with port range of \"All\"."
              - "#"
              - "# Trigger Type: Change Triggered"
              - "# Scope of Changes: EC2:SecurityGroup"
              - "# Accepted Parameters: None"
              - ""
              - "import boto3"
              - "import json"
              - ""
              - ""
              - "APPLICABLE_RESOURCES = [\"AWS::EC2::SecurityGroup\"]"
              - ""
              - ""
              - "def evaluate_compliance(configuration_item):"
              - ""
              - "    # Start as compliant"
              - "    compliance_type = 'COMPLIANT'"
              - "    annotation = \"Security group is compliant.\""
              - ""
              - "    # Check if resource was deleted"
              - "    if configuration_item['configurationItemStatus'] == \"ResourceDeleted\":"
              - "        compliance_type = 'NOT_APPLICABLE'"
              - "        annotation = \"This resource was deleted.\""
              - ""
              - "    # Check resource for applicability"
              - "    elif configuration_item[\"resourceType\"] not in APPLICABLE_RESOURCES:"
              - "        compliance_type = 'NOT_APPLICABLE'"
              - "        annotation = \"The rule doesn't apply to resources of type \"                      + configuration_item[\"resourceType\"] + \".\""
              - ""
              - "    else:"
              - "        # Iterate over IP permissions"
              - "        for i in configuration_item['configuration']['ipPermissions']:"
              - "            # inbound rules with no \"fromPort\" have a value of \"All\""
              - "            if \"fromPort\" not in i:"
              - "                compliance_type = 'NON_COMPLIANT'"
              - "                annotation = 'Security group is not compliant.'"
              - "                break"
              - ""
              - "    return {"
              - "        \"compliance_type\": compliance_type,"
              - "        \"annotation\": annotation"
              - "    }"
              - ""
              - ""
              - "def lambda_handler(event, context):"
              - ""
              - "    invoking_event = json.loads(event['invokingEvent'])"
              - "    configuration_item = invoking_event[\"configurationItem\"]"
              - "    evaluation = evaluate_compliance(configuration_item)"
              - "    config = boto3.client('config')"
              - ""
              - "    print('Compliance evaluation for %s: %s' % (configuration_item['resourceId'], evaluation[\"compliance_type\"]))"
              - "    print('Annotation: %s' % (evaluation[\"annotation\"]))"
              - ""
              - "    response = config.put_evaluations("
              - "       Evaluations=["
              - "           {"
              - "               'ComplianceResourceType': invoking_event['configurationItem']['resourceType'],"
              - "               'ComplianceResourceId':   invoking_event['configurationItem']['resourceId'],"
              - "               'ComplianceType':         evaluation[\"compliance_type\"],"
              - "               \"Annotation\":             evaluation[\"annotation\"],"
              - "               'OrderingTimestamp':      invoking_event['configurationItem']['configurationItemCaptureTime']"
              - "           },"
              - "       ],"
              - "       ResultToken=event['resultToken'])"
              - ""
      Timeout: 300
    DependsOn: "LambdaIamRoleCustomConfigRule"
  LambdaIamRoleCustomConfigRule:
    Type: "AWS::IAM::Role"
    Properties:
      RoleName: "IAMRoleForec2_security_group_port_range_all_prohibitedFuP"
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com"
            Action:
              - "sts:AssumeRole"
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
        - "arn:aws:iam::aws:policy/service-role/AWSConfigRulesExecutionRole"
        - "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
Parameters: {}
Metadata: {}
Conditions: {}
```

## EC2 Desired Instance Type Check<a name="ec2-desired-instances-type"></a>

A config rule that checks whether your EC2 instances are of the specified instance types.

This config rule supports Auto Remediation actions using SSM Automation. The following actions are supported:

- Stop Instance: Non-compliant instances are stopped.
- Terminate Instance: Non-compliant instances are terminated (Be careful when selecting this option to not accidentally terminate existing resources)\.

In addition to an action, a notification using an SNS Topic can be added to send a custom message when a non-compliant resource is detected. (Make sure to update the email address from the default email@example.com)
The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "desired-instance-type"
      Scope:
        ComplianceResourceTypes:
          - "AWS::EC2::Instance"
      Description: "A config rule that checks whether your EC2 instances are of the specified instance types."
      InputParameters:
        instanceType: "t2.small"
      Source:
        Owner: "AWS"
        SourceIdentifier: "DESIRED_INSTANCE_TYPE"
Parameters: {}
Metadata: {}
Conditions: {}
```

## EC2 Instances Managed by Systems Manager (SSM) Check<a name="ec2-managed-systems-manager"></a>

A Config rule that checks whether the Amazon EC2 instances in your account are managed by AWS Systems Manager\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "ec2-instance-managed-by-systems-manager"
      Scope:
        ComplianceResourceTypes:
          - "AWS::EC2::Instance"
          - "AWS::SSM::ManagedInstanceInventory"
      Description: "A Config rule that checks whether the Amazon EC2 instances in your account are managed by AWS Systems Manager."
      Source:
        Owner: "AWS"
        SourceIdentifier: "EC2_INSTANCE_MANAGED_BY_SSM"
Parameters: {}
Metadata: {}
Conditions: {}
```

## EC2 Instances Detailed Monitoring Enabled<a name="ec2-desired-detailed-monitoring"></a>

A Config rule that checks whether detailed monitoring is enabled for EC2 instances\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "ec2-instance-detailed-monitoring-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::EC2::Instance"
      Description: "A Config rule that checks whether detailed monitoring is enabled for EC2 instances."
      Source:
        Owner: "AWS"
        SourceIdentifier: "EC2_INSTANCE_DETAILED_MONITORING_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Unattached Elastic IPs (EIP) Check<a name="unattached-elastic-ip-check"></a>

A Config rule that checks whether all Elastic IP addresses that are allocated to a VPC are attached to EC2 instances or in-use elastic network interfaces (ENIs)\.

This config rule supports Auto Remediation actions using SSM Automation. The following actions are supported:

- Release Elastic IP: Unused Elastic IPs are released\.

In addition to an action, a notification using an SNS Topic can be added to send a custom message when a non-compliant resource is detected. (Make sure to update the email address from the default email@example.com)\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "eip-attached"
      Scope:
        ComplianceResourceTypes:
          - "AWS::EC2::EIP"
      Description: "A Config rule that checks whether all Elastic IP addresses that are allocated to a VPC are attached to EC2 instances or in-use elastic network interfaces (ENIs)."
      Source:
        Owner: "AWS"
        SourceIdentifier: "EIP_ATTACHED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## EC2 Instance Profile Attached<a name="ec2-instances-profile-attached"></a>

A Config rule that checks if an Amazon Elastic Compute Cloud (Amazon EC2) instance has an Identity and Access Management (IAM) profile attached to it. This rule is NON_COMPLIANT if no IAM profile is attached to the Amazon EC2 instance\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "ec2-instance-profile-attached"
      Scope:
        ComplianceResourceTypes:
          - "AWS::EC2::Instance"
      Description: "A Config rule that checks if an Amazon Elastic Compute Cloud (Amazon EC2) instance has an Identity and Access Management (IAM) profile attached to it. This rule is NON_COMPLIANT if no IAM profile is attached to the Amazon EC2 instance."
      Source:
        Owner: "AWS"
        SourceIdentifier: "EC2_INSTANCE_PROFILE_ATTACHED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## IAM Password Policy Settings Check<a name="iam-password-policy-settings-ports"></a>

A Config rule that checks whether the account password policy for IAM users meets the specified requirements\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "iam-password-policy"
      Scope:
        ComplianceResourceTypes: []
      Description: "A Config rule that checks whether the account password policy for IAM users meets the specified requirements."
      InputParameters:
        RequireUppercaseCharacters: "true"
        RequireLowercaseCharacters: "true"
        RequireSymbols: "true"
        RequireNumbers: "true"
        MinimumPasswordLength: "14"
        PasswordReusePrevention: "24"
        MaxPasswordAge: "90"
      Source:
        Owner: "AWS"
        SourceIdentifier: "IAM_PASSWORD_POLICY"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Root Account MFA Enabled Check<a name="root-account-mfa-enabled"></a>

A Config rule that checks whether users of your AWS account require a multi-factor authentication (MFA) device to sign in with root credentials.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "root-account-mfa-enabled"
      Scope:
        ComplianceResourceTypes: []
      Description: "A Config rule that checks whether users of your AWS account require a multi-factor authentication (MFA) device to sign in with root credentials."
      Source:
        Owner: "AWS"
        SourceIdentifier: "ROOT_ACCOUNT_MFA_ENABLED"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## IAM Users MFA Enabled Check<a name="iam-users-mfa-enabled"></a>

A config rule that checks whether the AWS Identity and Access Management users have multi-factor authentication (MFA) enabled.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "iam-user-mfa-enabled"
      Scope:
        ComplianceResourceTypes: []
      Description: "A config rule that checks whether the AWS Identity and Access Management users have multi-factor authentication (MFA) enabled."
      Source:
        Owner: "AWS"
        SourceIdentifier: "IAM_USER_MFA_ENABLED"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## IAM Policies don't allow Admin Access Check<a name="iam-policies-restrict-admin-access"></a>

A config rule that checks whether the default version of AWS Identity and Access Management (IAM) policies do not have administrator access. If any statement has 'Effect': 'Allow' with 'Action': '*' over 'Resource': '*', the rule is NON_COMPLIANT\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "iam-policy-no-statements-with-admin-access"
      Scope:
        ComplianceResourceTypes:
          - "AWS::IAM::Policy"
      Description: "A config rule that checks whether the default version of AWS Identity and Access Management (IAM) policies do not have administrator access. If any statement has 'Effect': 'Allow' with 'Action': '*' over 'Resource': '*', the rule is NON_COMPLIANT."
      Source:
        Owner: "AWS"
        SourceIdentifier: "IAM_POLICY_NO_STATEMENTS_WITH_ADMIN_ACCESS"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Root Access Keys Does Not Exist Check<a name="root-access-keys-does-not-exist"></a>

A config rule that checks whether the root user access key is available. The rule is COMPLIANT if the user access key does not exist\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "iam-root-access-key-check"
      Scope:
        ComplianceResourceTypes: []
      Description: "A config rule that checks whether the root user access key is available. The rule is COMPLIANT if the user access key does not exist."
      Source:
        Owner: "AWS"
        SourceIdentifier: "IAM_ROOT_ACCESS_KEY_CHECK"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## IAM Users Unused Credentials Check<a name="iam-users-unused-credentials"></a>

A config rule that checks whether your AWS Identity and Access Management (IAM) users have passwords or active access keys that have not been used within the specified number of days you provided. Re-evaluating this rule within 4 hours of the first evaluation will have no effect on the results\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "iam-user-unused-credentials-check"
      Scope:
        ComplianceResourceTypes: []
      Description: "A config rule that checks whether your AWS Identity and Access Management (IAM) users have passwords or active access keys that have not been used within the specified number of days you provided. Re-evaluating this rule within 4 hours of the first eva..."
      InputParameters:
        maxCredentialUsageAge: "90"
      Source:
        Owner: "AWS"
        SourceIdentifier: "IAM_USER_UNUSED_CREDENTIALS_CHECK"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Mfa Enabled for IAM Console Access<a name="mfa-enabled-iam-console-access"></a>

A Config rule that checks whether AWS Multi-Factor Authentication (MFA) is enabled for all AWS Identity and Access Management (IAM) users that use a console password. The rule is COMPLIANT if MFA is enabled\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "mfa-enabled-for-iam-console-access"
      Scope:
        ComplianceResourceTypes: []
      Description: "A Config rule that checks whether AWS Multi-Factor Authentication (MFA) is enabled for all AWS Identity and Access Management (IAM) users that use a console password. The rule is COMPLIANT if MFA is enabled."
      Source:
        Owner: "AWS"
        SourceIdentifier: "MFA_ENABLED_FOR_IAM_CONSOLE_ACCESS"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## S3 Bucket Logging Enabled Check<a name="s3-bucket-logging-enabled-ports"></a>

A Config rule that checks whether logging is enabled for your S3 buckets.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "s3-bucket-logging-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::S3::Bucket"
      Description: "A Config rule that checks whether logging is enabled for your S3 buckets."
      Source:
        Owner: "AWS"
        SourceIdentifier: "S3_BUCKET_LOGGING_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## S3 Bucket Public Read Disabled Check<a name="s3-bucket-public-read-disabled"></a>

A Config rule that checks that your Amazon S3 buckets do not allow public read access. If an Amazon S3 bucket policy or bucket ACL allows public read access, the bucket is noncompliant\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "s3-bucket-public-read-prohibited"
      Scope:
        ComplianceResourceTypes:
          - "AWS::S3::Bucket"
      Description: "A Config rule that checks that your Amazon S3 buckets do not allow public read access. If an Amazon S3 bucket policy or bucket ACL allows public read access, the bucket is noncompliant."
      Source:
        Owner: "AWS"
        SourceIdentifier: "S3_BUCKET_PUBLIC_READ_PROHIBITED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## S3 Bucket Public Write Disabled Check<a name="s3-bucket-public-write-disabled"></a>

A Config rule that checks that your Amazon S3 buckets do not allow public write access. If an Amazon S3 bucket policy or bucket ACL allows public write access, the bucket is noncompliant

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "s3-bucket-public-write-prohibited"
      Scope:
        ComplianceResourceTypes:
          - "AWS::S3::Bucket"
      Description: "A Config rule that checks that your Amazon S3 buckets do not allow public write access. If an Amazon S3 bucket policy or bucket ACL allows public write access, the bucket is noncompliant."
      Source:
        Owner: "AWS"
        SourceIdentifier: "S3_BUCKET_PUBLIC_WRITE_PROHIBITED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## S3 Bucket Server Side Encryption Enabled Check<a name="s3-bucket-server-side-encryption-enabled"></a>

A Config rule that checks that your Amazon S3 bucket either has Amazon S3 default encryption enabled or that the S3 bucket policy explicitly denies put-object requests without server side encryption.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "s3-bucket-server-side-encryption-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::S3::Bucket"
      Description: "A Config rule that checks that your Amazon S3 bucket either has Amazon S3 default encryption enabled or that the S3 bucket policy explicitly denies put-object requests without server side encryption."
      Source:
        Owner: "AWS"
        SourceIdentifier: "S3_BUCKET_SERVER_SIDE_ENCRYPTION_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## S3 Bucket Versioning Enabled Check<a name="s3-bucket-versioning-enabled"></a>

A Config rule that checks whether versioning is enabled for your S3 buckets\.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "s3-bucket-versioning-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::S3::Bucket"
      Description: "A Config rule that checks whether versioning is enabled for your S3 buckets. Optionally, the rule checks if MFA delete is enabled for your S3 buckets."
      Source:
        Owner: "AWS"
        SourceIdentifier: "S3_BUCKET_VERSIONING_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## S3 Bucket Default Encryption with KMS Enabled<a name="s3-bucket-Default-encryption-kms-enabled"></a>

A Config rule that checks whether the Amazon Simple Storage Service (Amazon S3) buckets are encrypted with AWS Key Management Service (AWS KMS). The rule is not NON_COMPLIANT if Amazon S3 bucket is not encrypted with AWS KMS key.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "s3-default-encryption-kms"
      Scope:
        ComplianceResourceTypes:
          - "AWS::S3::Bucket"
      Description: "A Config rule that checks whether the Amazon Simple Storage Service (Amazon S3) buckets are encrypted with AWS Key Management Service (AWS KMS). The rule is not NON_COMPLIANT if Amazon S3 bucket is not encrypted with AWS KMS key."
      Source:
        Owner: "AWS"
        SourceIdentifier: "S3_DEFAULT_ENCRYPTION_KMS"
Parameters: {}
Metadata: {}
Conditions: {}
```

## CloudTrail Data Events are Enabled for S3 Buckets Check<a name="cloudTrail-data-events-enabled-s3"></a>

A Config rule that checks whether at least one AWS CloudTrail trail is logging Amazon S3 data events for all S3 buckets. The rule is NON_COMPLIANT if trails that log data events for S3 buckets are not configured.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "cloudtrail-s3-dataevents-enabled"
      Scope:
        ComplianceResourceTypes: []
      Description: "A Config rule that checks whether at least one AWS CloudTrail trail is logging Amazon S3 data events for all S3 buckets. The rule is NON_COMPLIANT if trails that log data events for S3 buckets are not configured."
      Source:
        Owner: "AWS"
        SourceIdentifier: "CLOUDTRAIL_S3_DATAEVENTS_ENABLED"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## S3 Block Public Access Enabled (Account-Level)<a name="s3-block-public-access-enabled"></a>

A Config rule that checks whether the required public access block settings are configured from account level. The rule is only NON_COMPLIANT when the fields set below do not match the corresponding fields in the configuration item.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "s3-account-level-public-access-blocks"
      Scope:
        ComplianceResourceTypes:
          - "AWS::S3::AccountPublicAccessBlock"
      Description: "A Config rule that checks whether the required public access block settings are configured from account level. The rule is only NON_COMPLIANT when the fields set below do not match the corresponding fields in the configuration item."
      InputParameters:
        IgnorePublicAcls: "True"
        BlockPublicPolicy: "True"
        BlockPublicAcls: "True"
        RestrictPublicBuckets: "True"
      Source:
        Owner: "AWS"
        SourceIdentifier: "S3_ACCOUNT_LEVEL_PUBLIC_ACCESS_BLOCKS"
Parameters: {}
Metadata: {}
Conditions: {}
```

## S3 Lifecycle Policy Check<a name="s3-lifecycle-policy"></a>

A Config rule that checks if a lifecycle rule is configured for an Amazon Simple Storage Service (Amazon S3) bucket. The rule is NON_COMPLIANT if there is no active lifecycle configuration rules or the configuration does not match with the parameter values.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "s3-lifecycle-policy-check"
      Scope:
        ComplianceResourceTypes:
          - "AWS::S3::Bucket"
      Description: "A Config rule that checks if a lifecycle rule is configured for an Amazon Simple Storage Service (Amazon S3) bucket. The rule is NON_COMPLIANT if there is no active lifecycle configuration rules or the configuration does not match with the parameter va..."
      Source:
        Owner: "AWS"
        SourceIdentifier: "S3_LIFECYCLE_POLICY_CHECK"
Parameters: {}
Metadata: {}
Conditions: {}
```

## S3 Version Lifecycle Policy Check<a name="s3-version-lifecycle-policy"></a>

A Config rule that checks if Amazon Simple Storage Service (Amazon S3) version enabled buckets have lifecycle policy configured. The rule is NON_COMPLIANT if Amazon S3 lifecycle policy is not enabled.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "s3-version-lifecycle-policy-check"
      Scope:
        ComplianceResourceTypes:
          - "AWS::S3::Bucket"
      Description: "A Config rule that checks if Amazon Simple Storage Service (Amazon S3) version enabled buckets have lifecycle policy configured. The rule is NON_COMPLIANT if Amazon S3 lifecycle policy is not enabled."
      Source:
        Owner: "AWS"
        SourceIdentifier: "S3_VERSION_LIFECYCLE_POLICY_CHECK"
Parameters: {}
Metadata: {}
Conditions: {}
```

## RDS Storage Encrypted Check<a name="rds-storage-encrypted"></a>

A Config rule that checks whether storage encryption is enabled for your RDS DB instances

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "rds-storage-encrypted"
      Scope:
        ComplianceResourceTypes:
          - "AWS::RDS::DBInstance"
      Description: "A Config rule that checks whether storage encryption is enabled for your RDS DB instances."
      Source:
        Owner: "AWS"
        SourceIdentifier: "RDS_STORAGE_ENCRYPTED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## RDS Multi-AZ HA Enabled Check<a name="rds-multi-az-ha-enabled"></a>

A Config rule that checks whether high availability is enabled for your RDS DB instances. (Note: This rule does not evaluate Amazon Aurora databases.)

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "rds-multi-az-support"
      Scope:
        ComplianceResourceTypes:
          - "AWS::RDS::DBInstance"
      Description: "A Config rule that checks whether high availability is enabled for your RDS DB instances. (Note: This rule does not evaluate Amazon Aurora databases.)"
      Source:
        Owner: "AWS"
        SourceIdentifier: "RDS_MULTI_AZ_SUPPORT"
Parameters: {}
Metadata: {}
Conditions: {}
```

## No RDS Instances in Public Subnets Check<a name="rds-restrict-public-subnets"></a>

A Config rule that checks that no RDS Instances are in Public Subnet.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  CustomConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "rds_vpc_public_subnet"
      Scope:
        ComplianceResourceTypes:
          - "AWS::RDS::DBInstance"
      Description: "A Config rule that checks that no RDS Instances are in Public Subnet."
      Source:
        Owner: "CUSTOM_LAMBDA"
        SourceIdentifier:
          Fn::GetAtt:
            - "LambdaFunctionCustomConfigRule"
            - "Arn"
        SourceDetails:
          - EventSource: "aws.config"
            MessageType: "ConfigurationItemChangeNotification"
          - EventSource: "aws.config"
            MessageType: "OversizedConfigurationItemChangeNotification"
    DependsOn: "LambdaInvokePermissionsCustomConfigRule"
  LambdaInvokePermissionsCustomConfigRule:
    Type: "AWS::Lambda::Permission"
    Properties:
      FunctionName:
        Fn::GetAtt:
          - "LambdaFunctionCustomConfigRule"
          - "Arn"
      Action: "lambda:InvokeFunction"
      Principal: "config.amazonaws.com"
  LambdaFunctionCustomConfigRule:
    Type: "AWS::Lambda::Function"
    Properties:
      FunctionName: "LambdaForrds_vpc_public_subnet"
      Handler: "index.lambda_handler"
      Role:
        Fn::GetAtt:
          - "LambdaIamRoleCustomConfigRule"
          - "Arn"
      Runtime: "python3.9"
      Code:
        ZipFile:
          Fn::Join:
            - "\n"
            -
              - ""
              - "#"
              - "# This file made available under CC0 1.0 Universal (https://creativecommons.org/publicdomain/zero/1.0/legalcode)"
              - "#"
              - "# Description: Check that no RDS Instances are in Public Subnet"
              - "#"
              - "# Trigger Type: Change Triggered"
              - "# Scope of Changes: RDS:DBInstance"
              - "# Accepted Parameters: None"
              - ""
              - ""
              - "import boto3"
              - "import botocore"
              - "import json"
              - "import logging"
              - ""
              - "log = logging.getLogger()"
              - "log.setLevel(logging.INFO)"
              - ""
              - "def evaluate_compliance(configuration_item):"
              - "    vpc_id      = configuration_item[\"configuration\"]['dBSubnetGroup'][\"vpcId\"]"
              - "    subnet_ids   = []"
              - "    for i in configuration_item[\"configuration\"]['dBSubnetGroup']['subnets']:"
              - "        subnet_ids.append(i['subnetIdentifier'])"
              - "    client      = boto3.client(\"ec2\");"
              - ""
              - "    response    = client.describe_route_tables()"
              - ""
              - "    # If the subnet is explicitly associated to a route table, check if there"
              - "    # is a public route. If no explicit association exists, check if the main"
              - "    # route table has a public route."
              - ""
              - "    private = True"
              - ""
              - "    for subnet_id in subnet_ids:"
              - "        mainTableIsPublic = False"
              - "        noExplicitAssociationFound = True"
              - "        explicitAssocationIsPublic = False"
              - "        for i in response['RouteTables']:"
              - "            if i['VpcId'] == vpc_id:"
              - "                for j in i['Associations']:"
              - "                    if j['Main'] == True:"
              - "                        for k in i['Routes']:"
              - "                            if k['DestinationCidrBlock'] == '0.0.0.0/0' or k['GatewayId'].startswith('igw-'):"
              - "                                mainTableIsPublic = True"
              - "                    else:"
              - "                        if j['SubnetId'] == subnet_id:"
              - "                            noExplicitAssociationFound = False"
              - "                            for k in i['Routes']:"
              - "                                if k['DestinationCidrBlock'] == '0.0.0.0/0' or k['GatewayId'].startswith('igw-'):"
              - "                                    explicitAssocationIsPublic = True"
              - ""
              - "        if (mainTableIsPublic and noExplicitAssociationFound) or explicitAssocationIsPublic:"
              - "            private = False"
              - ""
              - "    if private:"
              - "        return {"
              - "            \"compliance_type\": \"COMPLIANT\","
              - "            \"annotation\": 'Its in private subnet'"
              - "        }"
              - "    else:"
              - "        return {"
              - "            \"compliance_type\" : \"NON_COMPLIANT\","
              - "            \"annotation\" : 'Not in private subnet'"
              - "        }"
              - ""
              - "def lambda_handler(event, context):"
              - "    log.debug('Event %s', event)"
              - "    invoking_event      = json.loads(event['invokingEvent'])"
              - "    configuration_item  = invoking_event[\"configurationItem\"]"
              - "    evaluation          = evaluate_compliance(configuration_item)"
              - "    config              = boto3.client('config')"
              - ""
              - "    response = config.put_evaluations("
              - "       Evaluations=["
              - "           {"
              - "               'ComplianceResourceType':    invoking_event['configurationItem']['resourceType'],"
              - "               'ComplianceResourceId':      invoking_event['configurationItem']['resourceId'],"
              - "               'ComplianceType':            evaluation[\"compliance_type\"],"
              - "               \"Annotation\":                evaluation[\"annotation\"],"
              - "               'OrderingTimestamp':         invoking_event['configurationItem']['configurationItemCaptureTime']"
              - "           },"
              - "       ],"
              - "       ResultToken=event['resultToken'])"
              - ""
      Timeout: 300
    DependsOn: "LambdaIamRoleCustomConfigRule"
  LambdaIamRoleCustomConfigRule:
    Type: "AWS::IAM::Role"
    Properties:
      RoleName: "IAMRoleForrds_vpc_public_subnetavA"
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com"
            Action:
              - "sts:AssumeRole"
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
        - "arn:aws:iam::aws:policy/service-role/AWSConfigRulesExecutionRole"
        - "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
Parameters: {}
Metadata: {}
Conditions: {}
```

## RDS Enhanced Monitoring Enabled<a name="rds-enhanced-monitoring-enabled"></a>

A config rule that checks whether enhanced monitoring is enabled for Amazon Relational Database Service (Amazon RDS) instances

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "rds-enhanced-monitoring-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::RDS::DBInstance"
      Description: "A config rule that checks whether enhanced monitoring is enabled for Amazon Relational Database Service (Amazon RDS) instances"
      Source:
        Owner: "AWS"
        SourceIdentifier: "RDS_ENHANCED_MONITORING_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## RDS Backup Enabled Check<a name="rds-backup-enabled"></a>

A config rule that checks whether RDS DB instances have backups enabled. Optionally, the rule checks the backup retention period and the backup window.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "db-instance-backup-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::RDS::DBInstance"
      Description: "A config rule that checks whether RDS DB instances have backups enabled. Optionally, the rule checks the backup retention period and the backup window."
      Source:
        Owner: "AWS"
        SourceIdentifier: "DB_INSTANCE_BACKUP_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## RDS Snapshot Encrypted Check<a name="rds-snapshot-encrypted"></a>

A config rule that checks whether Amazon Relational Database Service (Amazon RDS) DB snapshots are encrypted. The rule is NON_COMPLIANT, if Amazon RDS DB snapshots are not encrypted.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "rds-snapshot-encrypted"
      Scope:
        ComplianceResourceTypes:
          - "AWS::RDS::DBSnapshot"
          - "AWS::RDS::DBClusterSnapshot"
      Description: "A config rule that checks whether Amazon Relational Database Service (Amazon RDS) DB snapshots are encrypted. The rule is NON_COMPLIANT, if Amazon RDS DB snapshots are not encrypted."
      Source:
        Owner: "AWS"
        SourceIdentifier: "RDS_SNAPSHOT_ENCRYPTED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## RDS Cluster Deletion Protection Enabled<a name="rds-cluster-deletion-protection-enabled"></a>

A config rule that checks if an Amazon Relational Database Service (Amazon RDS) cluster has deletion protection enabled. This rule is NON_COMPLIANT if an RDS cluster does not have deletion protection enabled.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "rds-cluster-deletion-protection-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::RDS::DBCluster"
      Description: "A config rule that checks if an Amazon Relational Database Service (Amazon RDS) cluster has deletion protection enabled. This rule is NON_COMPLIANT if an RDS cluster does not have deletion protection enabled."
      Source:
        Owner: "AWS"
        SourceIdentifier: "RDS_CLUSTER_DELETION_PROTECTION_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## RDS Instance Deletion Protection Enabled<a name="rds-Instance-deletion-protection-enabled"></a>

A config rule that checks if an Amazon Relational Database Service (Amazon RDS) instance has deletion protection enabled. This rule is NON_COMPLIANT if an Amazon RDS instance does not have deletion protection enabled i.e deletionProtection is set to false.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "rds-instance-deletion-protection-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::RDS::DBInstance"
      Description: "A config rule that checks if an Amazon Relational Database Service (Amazon RDS) instance has deletion protection enabled. This rule is NON_COMPLIANT if an Amazon RDS instance does not have deletion protection enabled i.e deletionProtection is set to fa..."
      Source:
        Owner: "AWS"
        SourceIdentifier: "RDS_INSTANCE_DELETION_PROTECTION_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## CloudTrail Multi-Region Trail Enabled Check<a name="cloudTrail-multi-region-enabled"></a>

A config rule that checks that there is at least one multi-region AWS CloudTrail. The rule is NON_COMPLIANT if the trails do not match inputs parameters.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "multi-region-cloud-trail-enabled"
      Scope:
        ComplianceResourceTypes: []
      Description: "A config rule that checks that there is at least one multi-region AWS CloudTrail. The rule is NON_COMPLIANT if the trails do not match inputs parameters."
      Source:
        Owner: "AWS"
        SourceIdentifier: "MULTI_REGION_CLOUD_TRAIL_ENABLED"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```


## Amazon GuardDuty Enabled and Centralized<a name="amazon-guardDuty-enabled-centralized"></a>

A Config rule that checks whether Amazon GuardDuty is enabled in your AWS account and region. If you provide an AWS account for centralization, the rule evaluates the Amazon GuardDuty results in the centralized account. The rule is compliant when Amazon GuardDuty is enabled.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "guardduty-enabled-centralized"
      Scope:
        ComplianceResourceTypes: []
      Description: "A Config rule that checks whether Amazon GuardDuty is enabled in your AWS account and region. If you provide an AWS account for centralization, the rule evaluates the Amazon GuardDuty results in the centralized account. The rule is compliant when Amazo..."
      Source:
        Owner: "AWS"
        SourceIdentifier: "GUARDDUTY_ENABLED_CENTRALIZED"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## GuardDuty Untreated Findings Check<a name="guardDuty-untreated-findings"></a>

A Config rule that checks whether the Amazon GuardDuty has findings that are non archived. The rule is NON_COMPLIANT if Amazon GuardDuty has non archived low/medium/high severity findings older than the specified number in the daysLowSev/daysMediumSev/daysHighSev parameter.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "guardduty-non-archived-findings"
      Scope:
        ComplianceResourceTypes: []
      Description: "A Config rule that checks whether the Amazon GuardDuty has findings that are non archived. The rule is NON_COMPLIANT if Amazon GuardDuty has non archived low/medium/high severity findings older than the specified number in the daysLowSev/daysMediumSev/..."
      InputParameters:
        daysLowSev: "30"
        daysMediumSev: "7"
        daysHighSev: "1"
      Source:
        Owner: "AWS"
        SourceIdentifier: "GUARDDUTY_NON_ARCHIVED_FINDINGS"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## VPC Flow Logs Enabled Check<a name="vpc-flow-logs-enabled"></a>

A config rule that checks whether Amazon Virtual Private Cloud flow logs are found and enabled for Amazon VPC.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "vpc-flow-logs-enabled"
      Scope:
        ComplianceResourceTypes: []
      Description: "A config rule that checks whether Amazon Virtual Private Cloud flow logs are found and enabled for Amazon VPC."
      Source:
        Owner: "AWS"
        SourceIdentifier: "VPC_FLOW_LOGS_ENABLED"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## NACL Does Not Allow Unrestricted SSH or RDP Check<a name="NACL-does-not-allow-unrestricted-ssh-rdp"></a>

A Config rule that checks if default ports for SSH/RDP ingress traffic for network access control lists (NACLs) is unrestricted. The rule is NON_COMPLIANT if a NACL inbound entry allows a source TCP or UDP CIDR block for ports 22 or 3389.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "nacl-no-unrestricted-ssh-rdp"
      Scope:
        ComplianceResourceTypes:
          - "AWS::EC2::NetworkAcl"
      Description: "A Config rule that checks if default ports for SSH/RDP ingress traffic for network access control lists (NACLs) is unrestricted. The rule is NON_COMPLIANT if a NACL inbound entry allows a source TCP or UDP CIDR block for ports 22 or 3389."
      Source:
        Owner: "AWS"
        SourceIdentifier: "NACL_NO_UNRESTRICTED_SSH_RDP"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Security Hub Enabled<a name="security-hub-enabled"></a>

A config rule that checks that AWS Security Hub is enabled for an AWS account. The rule is NON_COMPLIANT if Security Hub is not enabled.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "securityhub-enabled"
      Scope:
        ComplianceResourceTypes: []
      Description: "A config rule that checks that AWS Security Hub is enabled for an AWS account. The rule is NON_COMPLIANT if Security Hub is not enabled."
      Source:
        Owner: "AWS"
        SourceIdentifier: "SECURITYHUB_ENABLED"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## CloudFormation Stack Notification Check<a name="cloudFormation-stack-notification"></a>

A config rule that checks whether your CloudFormation stacks are sending event notifications to an SNS topic. Optionally checks whether specified SNS topics are used.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "cloudformation-stack-notification-check"
      Scope:
        ComplianceResourceTypes:
          - "AWS::CloudFormation::Stack"
      Description: "A config rule that checks whether your CloudFormation stacks are sending event notifications to an SNS topic. Optionally checks whether specified SNS topics are used."
      InputParameters:
        snsTopic1: "sns-topic-1"
        snsTopic2: "sns-topic-2"
        snsTopic3: "sns-topic-3"
        snsTopic4: "sns-topic-4"
        snsTopic5: "sns-topic-5"
        snsTopic6: "sns-topic-6"
      Source:
        Owner: "AWS"
        SourceIdentifier: "CLOUDFORMATION_STACK_NOTIFICATION_CHECK"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Amazon Elasticsearch Encrypted at Rest<a name="elasticsearch-encrypted-at-rest"></a>

A Config rule that checks whether Amazon Elasticsearch Service (Amazon ES) domains have encryption at rest configuration enabled. The rule is NON_COMPLIANT if the EncryptionAtRestOptions field is not enabled.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "elasticsearch-encrypted-at-rest"
      Scope:
        ComplianceResourceTypes: []
      Description: "A Config rule that checks whether Amazon Elasticsearch Service (Amazon ES) domains have encryption at rest configuration enabled. The rule is NON_COMPLIANT if the EncryptionAtRestOptions field is not enabled."
      Source:
        Owner: "AWS"
        SourceIdentifier: "ELASTICSEARCH_ENCRYPTED_AT_REST"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Secrets Manager Rotation Enabled Check<a name="secrets-manager-rotation-enabled"></a>

A config rule that checks whether AWS Secrets Manager secret has rotation enabled. The rule also checks an optional maximumAllowedRotationFrequency parameter. If the parameter is specified, the rotation frequency of the secret is compared with the maximum allowed frequency.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "secretsmanager-rotation-enabled-check"
      Scope:
        ComplianceResourceTypes:
          - "AWS::SecretsManager::Secret"
      Description: "A config rule that checks whether AWS Secrets Manager secret has rotation enabled. The rule also checks an optional maximumAllowedRotationFrequency parameter. If the parameter is specified, the rotation frequency of the secret is compared with the maxi..."
      InputParameters:
        maximumAllowedRotationFrequency: "7"
      Source:
        Owner: "AWS"
        SourceIdentifier: "SECRETSMANAGER_ROTATION_ENABLED_CHECK"
Parameters: {}
Metadata: {}
Conditions: {}
```

## Account Part of AWS Organizations Check<a name="account-part-aws-organizations"></a>

A Config rule that checks whether AWS account is part of AWS Organizations. The rule is NON_COMPLIANT if an AWS account is not part of AWS Organizations or AWS Organizations master account ID does not match rule parameter MasterAccountId.

The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "account-part-of-organizations"
      Scope:
        ComplianceResourceTypes: []
      Description: "A Config rule that checks whether AWS account is part of AWS Organizations. The rule is NON_COMPLIANT if an AWS account is not part of AWS Organizations or AWS Organizations master account ID does not match rule parameter MasterAccountId."
      InputParameters:
        MasterAccountId: "MasterAccountIdValue???"
      Source:
        Owner: "AWS"
        SourceIdentifier: "ACCOUNT_PART_OF_ORGANIZATIONS"
      MaximumExecutionFrequency: "TwentyFour_Hours"
Parameters: {}
Metadata: {}
Conditions: {}
```

## ECR Private Image Scanning Enabled<a name="ecr-private-image-scanning"></a>

A Config rule that checks if a private Amazon Elastic Container Registry (ECR) repository has image scanning enabled. The rule is NON_COMPLIANT if image scanning is not enabled for the private ECR repository.
The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "ecr-private-image-scanning-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::ECR::Repository"
      Description: "A Config rule that checks if a private Amazon Elastic Container Registry (ECR) repository has image scanning enabled. The rule is NON_COMPLIANT if image scanning is not enabled for the private ECR repository."
      Source:
        Owner: "AWS"
        SourceIdentifier: "ECR_PRIVATE_IMAGE_SCANNING_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

## ECR Private Tag Immutability Enabled<a name="ecr-private-Immutability-enabled"></a>

A Config rule that checks if a private Amazon Elastic Container Registry (ECR) repository has tag immutability enabled. This rule is NON_COMPLIANT if tag immutability is not enabled for the private ECR repository.
The artifact for this control is the following AWS Config rule\.

```
AWSTemplateFormatVersion: "2010-09-09"
Description: ""
Resources:
  ConfigRule:
    Type: "AWS::Config::ConfigRule"
    Properties:
      ConfigRuleName: "ecr-private-tag-immutability-enabled"
      Scope:
        ComplianceResourceTypes:
          - "AWS::ECR::Repository"
      Description: "A Config rule that checks if a private Amazon Elastic Container Registry (ECR) repository has tag immutability enabled. This rule is NON_COMPLIANT if tag immutability is not enabled for the private ECR repository."
      Source:
        Owner: "AWS"
        SourceIdentifier: "ECR_PRIVATE_TAG_IMMUTABILITY_ENABLED"
Parameters: {}
Metadata: {}
Conditions: {}
```

