
#!/bin/bash

set -ex

#git clone https://git-codecommit.us-east-1.amazonaws.com/v1/repos/custom-control-tower-configuration

#git clone https://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@github.com/daaops/control_tower_with_manifest.git

#rm -rf control_tower_with_manifest

#cp /home/ec2-user/environment/control_tower_with_manifest/script/clone_n_replace.sh . 

#sh /home/ec2-user/environment/control_tower_with_manifest/script/clone_n_replace.sh

rm -rf /home/ec2-user/environment/control_tower_with_manifest

cd /home/ec2-user/environment

git clone https://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@github.com/daaops/control_tower_with_manifest.git

#cd control_tower_with_manifest/script/
#sh clone_n_replace.sh

cd /home/ec2-user/environment/custom-control-tower-configuration

rm -rf example-configuration manifest.yaml

cd ../control_tower_with_manifest/custom-control-tower-configuration

cp -R . ../../custom-control-tower-configuration/

cd /home/ec2-user/environment/custom-control-tower-configuration

pwd 

sleep 5

git status

git add . && git commit -am "updated" &&  git push 