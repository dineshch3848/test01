
#!/bin/bash

set -ex

bucketname=$1

#aws codecommit delete-repository --repository-name custom-control-tower-configuration

aws s3api delete-objects --bucket $bucketname --delete "$(aws s3api list-object-versions --bucket "$bucketname" --output=json --query='{Objects: Versions[].{Key:Key,VersionId:VersionId}}')"

echo "Deleteing bucket"

aws s3 rb s3://$bucketname