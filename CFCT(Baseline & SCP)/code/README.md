# cfct_with_manifest

This repo contains sample Service Control Policies (SCPs) and CloudFormation templates for customizing a Landing Zone in AWS Control Tower as part of the [Customizations for AWS Control Tower](https://aws.amazon.com/solutions/implementations/customizations-for-aws-control-tower/) solution by AWS.

The SCPs, CloudFormation templates, and a [manifest file](manifest.yaml) will be bundled as a Configuration Package in a zip file for deployment to the Landing Zone in Control Tower. To deploy the Configuration File, the zip file will be uploaded to an S3 bucket created as part of the Customizations for AWS Control Tower solution. The AWS CodePipeline will then deploy SCPs and CloudFormation StackSets to appropriate member AWS Accounts.

The following diagram shows architecture of the Customizations for AWS Control Tower solution

![Customizations for AWS Control Tower solution](https://d1.awsstatic.com/aws-answers/answers-images/customizations-for-aws-control-tower-architecture-diagram.00c3c6e503a5254fe8990d5d952f3bcf3d60d077.png)

## Structure of Repo as follow :- 

- custom-control-tower-configuration
	- example-configuration
		- parameters
		- policies :- for applying all SCP
		- templates :- for applying all baseline
		- manifest.yaml
	- manifest.yaml

- customizations-for-aws-control-tower.template


## Some Important terminology

- **SCP**

1. SCPs are created in AWS Organizations
2. These SCPs are then applied to Organizational Units (OUs). 

***Note***  :- In our repo we are maintaing SCP via policy directory structure.

- **Baseline**

1. Baseline are sets up AWS account with the secure baseline configuration based on Center for Internet Security (CIS) Amazon Web Services Foundations.
2. AWS Control Tower Baseline is designed to provide customers with a secure and compliant AWS environment that is easy to manage and scale. It enables customers to implement best practices quickly and easily and ensures that their AWS environment is configured in a consistent and standardized manner.

***Note*** :- In our repo we are maintaing Baseline via templates directory structure.

## Limitations

### SCP
- [SCP Service Limits](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_reference_limits.html) for SCPs. 
- SCPs are a preventative control and can have unintended consequences. Ensure you have a good knowledge of AWS Organizations and Terraform before you deploy this pattern, especially to production environments.
- This pattern defaults to SCPs for Root, Sandbox, and Workload. But this can be fully customized.
	1. SCPs are created in AWS Organizations
	2. These SCPs are then applied to Organizational Units (OUs). 


### Guardrails
- [Proactive Guardrails](https://repost.aws/questions/QUTK5bhzd1SXysdqTkvWavnQ/can-i-create-custom-control-tower-proactive-controls), Custom controls are not yet supported [SCP Service Limits](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_reference_limits.html)


## Getting Started 
To get started with Customizations for AWS Control Tower, please review the [documentation](https://docs.aws.amazon.com/controltower/latest/userguide/customize-landing-zone.html)

1. Clone repo and manaully import **customizations-for-aws-control-tower.template** into cloudformation.

2. In Specify stack details page update below parameters and leave the remaining as defaults.
	- Select :- AWS CodePipeline Source: AWS CodeCommit.

3. Review page, select **I acknowledge that AWS CloudFormation might create IAM resources with custom names.** and choose **Create stack**	

4. As after deployment of above template successfully into your aws account. We can move fuhter to apply *SCP* and *Baseline* on some of OU.

5. Required SCP and Baseline can be applied by making changes on manifest.yaml which is present inside custom-control-tower-configuration directory.Connect to the **CodeCommit Repository** remotely use **Cloud9** Environment.
	-	Navigate to **Cloud9** Console, and select **Create** environment
	-	Type in appropriate Name and Description to choose on Next step
	-	Pick following options in Environment settings and choose Next step
		- Create a new instance for environment (EC2)
		- t2.micro (1 Gib RAM + 1 vCPU)
		-	Amazon Linux
	- Choose **Create** environment

6. Some of Basic configuration is already present, just be cautious on directory path as whatever default SCP and Baseline you need. 

7. After finalizing of SCP and Baseline that you need to apply on OU. You just need to push this all this repository code to code commit.Replace the sample manifest.yaml file in the root of your CodeCommit repository with the following:
***Note :- Maintain the directory structre as you gonna replace the code with our following changes.***
```
---
region: us-east-1
version: 2020-01-01

# Control Tower Custom Service Control Policies
organization_policies:
  - name: DenyAccountRegionDisableEnable
   	description: DenyAccountRegionDisableEnable
  	policy_file: example-configuration/policies/ec2-policies.json
    apply_to_accounts_in_ou:      
      - developers
  - name: DenyS3DeleteBucketsAndObjects S3BucketsPublicAccess and S3IncorrectEncryptionHeader
    description: DenyS3DeleteBucketsAndObjects S3BucketsPublicAccess and S3IncorrectEncryptionHeader
    policy_file: example-configuration/policies/s3.json
    apply_to_accounts_in_ou:
      - developers     
  - name: DenyVpcDeletingFlowLogs and InternetAccess
    description: DenyVpcDeletingFlowLogs and InternetAccess
    policy_file: example-configuration/policies/vpc-policies.json
    apply_to_accounts_in_ou:
      - DevOps

# Control Tower Custom CloudFormation Resources
cloudformation_resources:
  - name: SecretManager
    template_file: example-configuration/templates/secret-manager-baseline/SecretManager.yaml
    deploy_method: stack_set
    deploy_to_ou:
      - developers
    regions:
      - us-east-1  
  - name: SystemsManager
    template_file: example-configuration/templates/SystemsManager.yaml
    deploy_method: stack_set
    deploy_to_ou:
      - DevOps
    regions:
      - us-east-1      

```	

8. You can take reference of SCP from following directory i.e.control_tower_with_manifest/[custom-control-tower-configuration/example-configuration/policies/Readme.md](custom-control-tower-configuration/example-configuration/policies/Readme.md). A full description of policy is mention whatever you would like to apply.

9. Same for Baseline as well, refer [custom-control-tower-configuration/example-configuration/templates/Readme.md](custom-control-tower-configuration/example-configuration/templates/Readme.md)

10. Finally, just after push code to code commit it will start Code pipeline with certain COde Deploy stage and your baseline and scp will get relected to OU account.

11. To check whatever SCP and Baseline is applied, you must login in OU account and check cloudformation service for required baseline and SCP.
