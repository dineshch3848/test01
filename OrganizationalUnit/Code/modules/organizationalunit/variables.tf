variable "default_tags" {
  description = "Additional resource tags"
  type        = map(string)
}

variable "organizational_units" {
  type = list(object({
    name      = string
    parent_id = string
  }))
  default     = []
  description = <<EOT
    organizational_units = [{
      name: "Name of the Organizational Unit"
      parent_id: "Id of the Parent Organizational Unit"
    }]
  EOT  
}