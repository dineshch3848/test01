/*
  * # AWS Organizational Unit Resource
  *
  * This resource handles the creation of Organizational Units .
  * 
  */

resource "aws_organizations_organizational_unit" "this" {
  for_each  = { for key, value in var.organizational_units : key => value }
  name      = each.value.name
  parent_id = each.value.parent_id
  tags = merge(
    var.default_tags,
    {
      Name = each.value.name
    }
  )
}