output "organizational_unit_id" {
  value       = [for val in aws_organizations_organizational_unit.this : val.id]
  description = "ID of the OUs created"
}