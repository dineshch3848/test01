default_tags = {
  "Owner" = "siddharth.mishra@yash.com"
}

organizational_units = [{
  name      = "IMS"
  parent_id = "r-aiar"
  },
  {
    name      = "Mobility"
    parent_id = "r-aiar"
  },
  {
    name      = "IMS-FinOps"
    parent_id = "ou-aiar-x3z0uol9"
  }
]