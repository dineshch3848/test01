output "organizational_unit_id" {
  value       = module.organizational_unit.organizational_unit_id
  description = "ID of the OUs created"
}