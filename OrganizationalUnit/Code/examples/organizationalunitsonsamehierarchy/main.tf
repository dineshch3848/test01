/*
  * # AWS Organizational Unit Module
  *
  * This module handles the creation of Organizational Units .
  * 
  */

module "organizational_unit" {
  source               = "../../modules/organizationalunit"
  organizational_units = var.organizational_units
  default_tags         = var.default_tags
}