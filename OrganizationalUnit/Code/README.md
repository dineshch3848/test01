<!-- BEGIN_TF_DOCS -->
# AWS Organizational Unit Module

This module handles the creation of Organizational Units .

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_organizational_unit"></a> [organizational\_unit](#module\_organizational\_unit) | ./modules/organizationalunit | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | Additional resource tags | `map(string)` | n/a | yes |
| <a name="input_organizational_units"></a> [organizational\_units](#input\_organizational\_units) | organizational\_unit = [{<br>      name: "Name of the Organizational Unit"<br>      parent\_id: "Id of the Parent Organizational Unit"<br>    }] | <pre>list(object({<br>    name      = string<br>    parent_id = string<br>  }))</pre> | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_organizational_unit_id"></a> [organizational\_unit\_id](#output\_organizational\_unit\_id) | ID of the OUs created |
<!-- END_TF_DOCS -->