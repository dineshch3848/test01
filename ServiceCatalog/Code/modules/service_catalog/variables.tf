variable "region" {
  type    = string
  default = "us-east-1"
}

variable "default_tags" {
  type = map(string)
}

variable "create_portfolio" {
  type = bool
}

variable "portfolio_name" {
  type    = string
  default = ""
}

variable "portfolio_description" {
  type    = string
  default = ""
}

variable "provider_name" {
  type    = string
  default = ""
}

variable "portfolio_id" {
  type    = string
  default = ""
}

variable "servicecatalog_products" {
  type = list(object({
    product_name  = string
    product_owner = string
    product_type  = string
    template_url  = string
    template_type = string
  }))
  default = []
}

variable "portfolio_share" {
  type = list(object({
    principal_id   = string
    principal_type = string
  }))
  default = []
}