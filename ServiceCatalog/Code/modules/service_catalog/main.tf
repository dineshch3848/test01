resource "aws_servicecatalog_portfolio" "this" {
  count         = var.create_portfolio ? 1 : 0
  name          = var.portfolio_name
  description   = var.portfolio_description
  provider_name = var.provider_name
  tags          = var.default_tags
}

resource "aws_servicecatalog_product" "this" {
  for_each = { for key, value in var.servicecatalog_products : key => value }
  name     = each.value.product_name
  owner    = each.value.product_owner
  type     = each.value.product_type

  provisioning_artifact_parameters {
    template_url = each.value.template_url
    type         = each.value.template_type
  }
  tags = var.default_tags
}

resource "aws_servicecatalog_product_portfolio_association" "this" {
  for_each     = { for key, value in aws_servicecatalog_product.this : key => value }
  portfolio_id = var.create_portfolio ? aws_servicecatalog_portfolio.this[0].id : var.portfolio_id
  product_id   = each.value.id
}

resource "aws_servicecatalog_portfolio_share" "this" {
  for_each     = { for key, value in var.portfolio_share : key => value }
  principal_id = each.value.principal_id
  portfolio_id = var.create_portfolio ? aws_servicecatalog_portfolio.this[0].id : var.portfolio_id
  type         = each.value.principal_type
}