output "servicecatalog_products_id" {
  value = { for key, value in aws_servicecatalog_product.this : value.name => value.id }
}

output "servicecatalog_portfolio_id" {
  value = var.create_portfolio ? aws_servicecatalog_portfolio.this[0].id : var.portfolio_id
}