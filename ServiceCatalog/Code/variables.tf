variable "region" {
  type        = string
  default     = "us-east-1"
  description = "Region where infrastructure would be created"
}

variable "default_tags" {
  type        = map(string)
  description = "Default tags of the module"
}

variable "create_portfolio" {
  type        = bool
  description = "Whether portfolio to be created"
}

variable "portfolio_name" {
  type    = string
  default = "If create_portfolio=true, provide portfolio name"
}

variable "portfolio_description" {
  type    = string
  default = "If create_portfolio=true, provide portfolio description"
}

variable "provider_name" {
  type    = string
  default = "If create_portfolio=true, provide provider name"
}

variable "portfolio_id" {
  type    = string
  default = "If create_portfolio=false, provide id of the portfolio to which products would be attached"
}

variable "servicecatalog_products" {
  type = list(object({
    product_name  = string
    product_owner = string
    product_type  = string
    template_url  = string
    template_type = string
  }))
  default     = []
  description = <<EOT
    servicecatalog_products = [{
      product_name : "Name of the service catalog product"
      product_owner: "Owner name of the service catalog product"
      product_type: "Type of product, valid values are CLOUD_FORMATION_TEMPLATE | MARKETPLACE | TERRAFORM_OPEN_SOURCE"
      template_url: "URL of the template put in s3 bucket"
      template_type: "Type of template, valid values are CLOUD_FORMATION_TEMPLATE | MARKETPLACE_AMI | MARKETPLACE_CAR | TERRAFORM_OPEN_SOURCE"
    }]
  EOT
}

variable "portfolio_share" {
  type = list(object({
    principal_id   = string
    principal_type = string
  }))
  default     = []
  description = <<EOT
    portfolio_share = [{
      principal_id : "Identifier of the principal with whom you will share the portfolio. Valid values AWS account IDs and ARNs of AWS Organizations and organizational units"
      principal_type: "Type of portfolio share. Valid values are ACCOUNT (an external account), ORGANIZATION (a share to every account in an organization), ORGANIZATIONAL_UNIT, ORGANIZATION_MEMBER_ACCOUNT (a share to an account in an organization)"
    }]
  EOT  
}