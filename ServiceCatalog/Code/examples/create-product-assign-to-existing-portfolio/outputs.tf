output "servicecatalog_products_id" {
  value = module.service_catalog.servicecatalog_products_id #{ for key, value in aws_servicecatalog_product.this : value.name => value.id }
}

output "servicecatalog_portfolio_id" {
  value = module.service_catalog.servicecatalog_portfolio_id #aws_servicecatalog_portfolio.this[0].id
}