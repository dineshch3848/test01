module "service_catalog" {
  source                  = "../../modules/service_catalog"
  region                  = var.region
  default_tags            = var.default_tags
  create_portfolio        = var.create_portfolio
  portfolio_id            = var.portfolio_id
  servicecatalog_products = var.servicecatalog_products
  portfolio_share         = var.portfolio_share
}