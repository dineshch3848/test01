default_tags = {
  "Business Unit / Cost Center" = "IT Infrastructure"
  "Project Name"                = "Landing Zone Automation"
  "Environment"                 = "Production"
}
region                = "eu-west-1"
portfolio_name        = "test-one"
portfolio_description = "test portfolio"
provider_name         = "Yash DevOps Team"
create_portfolio      = true
servicecatalog_products = [{
  product_name  = "vpc-provisioner"
  product_owner = "Yash DevOps Team"
  product_type  = "CLOUD_FORMATION_TEMPLATE"
  template_type = "CLOUD_FORMATION_TEMPLATE"
  template_url  = "https://test-bucket-ct-yash.s3.eu-west-1.amazonaws.com/vpc-with-public-subnets.yml"
  },
  {
    product_name  = "s3-provisioner"
    product_owner = "Yash DevOps Team"
    product_type  = "CLOUD_FORMATION_TEMPLATE"
    template_type = "CLOUD_FORMATION_TEMPLATE"
    template_url  = "https://test-bucket-ct-yash.s3.eu-west-1.amazonaws.com/s3-tf-bucket.yml"
  }
]

portfolio_share = [
  {
    principal_id   = "arn:aws:organizations::560979642322:ou/o-m0pshonbbg/ou-aiar-d4okj5i3"
    principal_type = "ORGANIZATIONAL_UNIT"
  },
  # {
  #   principal_id   = "arn:aws:organizations::560979642322:organization/o-m0pshonbbg"
  #   principal_type = "ORGANIZATION"
  # }
  {
    principal_id   = "arn:aws:organizations::560979642322:ou/o-m0pshonbbg/ou-aiar-bmnal3v2"
    principal_type = "ORGANIZATIONAL_UNIT"
  }
]