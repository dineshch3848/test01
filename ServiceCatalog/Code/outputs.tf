output "servicecatalog_products_id" {
  value = module.service_catalog.servicecatalog_products_id
  description = "service catalog product id"
}

output "servicecatalog_portfolio_id" {
  value = module.service_catalog.servicecatalog_portfolio_id
  description = "service catalog portfolio id"
}