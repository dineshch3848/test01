<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_service_catalog"></a> [service\_catalog](#module\_service\_catalog) | ./modules/service_catalog | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_portfolio"></a> [create\_portfolio](#input\_create\_portfolio) | Whether portfolio to be created | `bool` | n/a | yes |
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | Default tags of the module | `map(string)` | n/a | yes |
| <a name="input_portfolio_description"></a> [portfolio\_description](#input\_portfolio\_description) | n/a | `string` | `"If create_portfolio=true, provide portfolio description"` | no |
| <a name="input_portfolio_id"></a> [portfolio\_id](#input\_portfolio\_id) | n/a | `string` | `"If create_portfolio=false, provide id of the portfolio to which products would be attached"` | no |
| <a name="input_portfolio_name"></a> [portfolio\_name](#input\_portfolio\_name) | n/a | `string` | `"If create_portfolio=true, provide portfolio name"` | no |
| <a name="input_portfolio_share"></a> [portfolio\_share](#input\_portfolio\_share) | portfolio\_share = [{<br>      principal\_id : "Identifier of the principal with whom you will share the portfolio. Valid values AWS account IDs and ARNs of AWS Organizations and organizational units"<br>      principal\_type: "Type of portfolio share. Valid values are ACCOUNT (an external account), ORGANIZATION (a share to every account in an organization), ORGANIZATIONAL\_UNIT, ORGANIZATION\_MEMBER\_ACCOUNT (a share to an account in an organization)"<br>    }] | <pre>list(object({<br>    principal_id   = string<br>    principal_type = string<br>  }))</pre> | `[]` | no |
| <a name="input_provider_name"></a> [provider\_name](#input\_provider\_name) | n/a | `string` | `"If create_portfolio=true, provide provider name"` | no |
| <a name="input_region"></a> [region](#input\_region) | Region where infrastructure would be created | `string` | `"us-east-1"` | no |
| <a name="input_servicecatalog_products"></a> [servicecatalog\_products](#input\_servicecatalog\_products) | servicecatalog\_products = [{<br>      product\_name : "Name of the service catalog product"<br>      product\_owner: "Owner name of the service catalog product"<br>      product\_type: "Type of product, valid values are CLOUD\_FORMATION\_TEMPLATE \| MARKETPLACE \| TERRAFORM\_OPEN\_SOURCE"<br>      template\_url: "URL of the template put in s3 bucket"<br>      template\_type: "Type of template, valid values are CLOUD\_FORMATION\_TEMPLATE \| MARKETPLACE\_AMI \| MARKETPLACE\_CAR \| TERRAFORM\_OPEN\_SOURCE"<br>    }] | <pre>list(object({<br>    product_name  = string<br>    product_owner = string<br>    product_type  = string<br>    template_url  = string<br>    template_type = string<br>  }))</pre> | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_servicecatalog_portfolio_id"></a> [servicecatalog\_portfolio\_id](#output\_servicecatalog\_portfolio\_id) | service catalog portfolio id |
| <a name="output_servicecatalog_products_id"></a> [servicecatalog\_products\_id](#output\_servicecatalog\_products\_id) | service catalog product id |
<!-- END_TF_DOCS -->